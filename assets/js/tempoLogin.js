tempo = Number();
tempo = 900;

function startCountdown() {
    if ((tempo - 1) >= 0) {
        var min = parseInt(tempo / 60);
        var seg = tempo % 60;
        if (min < 10) {
            min = "0" + min;
            min = min.substr(0, 2);
        }
        if (seg <= 9) {
            seg = "0" + seg;
        }
        horaImprimivel = '00:' + min + ':' + seg;
        $("#sessaoT").html(horaImprimivel);
        // console.log(horaImprimivel);
        setTimeout('startCountdown()', 1000);
        tempo--;
    } else {
        window.open('home', '_self');
    }
}

startCountdown();
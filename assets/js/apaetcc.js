function modalLogarS() {
    jQuery.ajax({
        url: 'logarS',
        type: 'POST',
        data: {
            "modalLogar": 'LoginSistem',
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            document.getElementById('formLogar').innerHTML = data;
            $('#logarModal').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function logoutClicado() {
    jQuery.ajax({
        url: 'logoutClick',
        type: 'POST',
        data: {
            logoutClickValor: 1,
        },
        dataType: 'json',
        success: function (data, textStatus, xhr) {
            console.log(data); // do with data e.g success message
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function modalCadastrarUser() {
    jQuery.ajax({
        url: 'cadUser',
        type: 'POST',
        data: {
            "value": 'cadastrar',
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            document.getElementById('formModal').innerHTML = data;
            $('#cadastarUsuarioModal').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function modalUpdateUser(url) {
    var table = document.getElementById('tableUsuario');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var usuario_id = this.cells[0].innerHTML;
            var usuario_idNivel = this.cells[1].innerHTML;
            var usuario_nome = this.cells[2].innerHTML;
            var usuario_email = this.cells[3].innerHTML;
            var usuario_senha = this.cells[4].innerHTML;
            var usuario_perfil = this.cells[5].innerHTML;
            var usuario_habilitado = this.cells[6].innerHTML;
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    "usuario_id": usuario_id,
                    "usuario_idNivel": usuario_idNivel,
                    "usuario_nome": usuario_nome,
                    "usuario_email": usuario_email,
                    "usuario_senha": usuario_senha,
                    "usuario_perfil": usuario_perfil,
                    "usuario_habilitado": usuario_habilitado,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    document.getElementById('formModalUp').innerHTML = data;
                    $('#updateUsuarioModal').modal('show');
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}

function habilitaUser(url) {
    var table = document.getElementById('tableUsuario');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var usuario_id = this.cells[0].innerHTML;
            var usuario_idNivel = this.cells[1].innerHTML;
            var usuario_nome = this.cells[2].innerHTML;
            var usuario_email = this.cells[3].innerHTML;
            var usuario_senha = this.cells[4].innerHTML;
            var usuario_perfil = this.cells[5].innerHTML;
            var usuario_habilitado = this.cells[6].innerHTML;
            var data = {
                "usuario_id": usuario_id,
                "usuario_idNivel": usuario_idNivel,
                "usuario_nome": usuario_nome,
                "usuario_email": usuario_email,
                "usuario_senha": usuario_senha,
                "usuario_perfil": usuario_perfil,
                "usuario_habilitado": usuario_habilitado,
            };
            // if (confirm("Habilitar/Desabilitar usuário !? \n" + usuario_nome)) {
            //     console.log("You pressed Ok!");
            var form = document.createElement('form');
            form.method = 'POST';
            form.target = '_self';
            form.action = 'hbUser';
            for (var key in data) {
                var input = document.createElement('input');
                input.name = key;
                input.value = data[key];
                input.type = 'hidden';
                form.appendChild(input)
            }
            document.body.appendChild(form);
            form.submit();
            // } else {
            //     console.log("You pressed Cancel!");
            // }
        };
    }
}

function modalCadastrarDisciplina() {
    jQuery.ajax({
        url: 'cadDiscip',
        type: 'POST',
        data: {
            "value": 'cadastrar',
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            document.getElementById('formModalDiscip').innerHTML = data;
            $('#cadastarDisciplinaModal').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function modalUpdateDisciplina(url) {
    var table = document.getElementById('tableDisciplina');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var disciplina_id = this.cells[0].innerHTML;
            var usuario_id = this.cells[1].innerHTML;
            var disciplina_nome = this.cells[2].innerHTML;
            var disciplina_docente = this.cells[3].innerHTML;
            var disciplina_habilitado = this.cells[4].innerHTML;
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    "disciplina_id": disciplina_id,
                    "usuario_id": usuario_id,
                    "disciplina_nome": disciplina_nome,
                    "disciplina_docente": disciplina_docente,
                    "disciplina_habilitado": disciplina_habilitado,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    document.getElementById('formModalDiscipUp').innerHTML = data;
                    $('#updateDisciplinaModal').modal('show');
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}

function habilitaDisciplina(url) {
    var table = document.getElementById('tableDisciplina');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var disciplina_id = this.cells[0].innerHTML;
            var usuario_id = this.cells[1].innerHTML;
            var disciplina_nome = this.cells[2].innerHTML;
            var disciplina_docente = this.cells[3].innerHTML;
            var disciplina_habilitado = this.cells[4].innerHTML;
            var data = {
                "disciplina_id": disciplina_id,
                "usuario_id": usuario_id,
                "disciplina_nome": disciplina_nome,
                "disciplina_docente": disciplina_docente,
                "disciplina_habilitado": disciplina_habilitado,
            };
            var form = document.createElement('form');
            form.method = 'POST';
            form.target = '_self';
            form.action = url;
            for (var key in data) {
                var input = document.createElement('input');
                input.name = key;
                input.value = data[key];
                input.type = 'hidden';
                form.appendChild(input)
            }
            document.body.appendChild(form);
            form.submit();
        };
    }
}

function modalCadastrarAluno() {
    jQuery.ajax({
        url: 'cadAluno',
        type: 'POST',
        data: {
            "value": 'cadastrar',
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            document.getElementById('formModalAluno').innerHTML = data;
            $('#cadastarAlunoModal').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function modalUpdateAluno(url) {
    var table = document.getElementById('tableAluno');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var aluno_id = this.cells[0].innerHTML;
            var aluno_ra = this.cells[1].innerHTML;
            var aluno_nome = this.cells[2].innerHTML;
            var aluno_telefone = this.cells[3].innerHTML;
            var aluno_aula = this.cells[4].innerHTML;
            var aluno_habilitado = this.cells[5].innerHTML;
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    "aluno_id": aluno_id,
                    "aluno_ra": aluno_ra,
                    "aluno_nome": aluno_nome,
                    "aluno_telefone": aluno_telefone,
                    "aluno_aula": aluno_aula,
                    "aluno_habilitado": aluno_habilitado,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    document.getElementById('formModalAlunoUp').innerHTML = data;
                    $('#updateAlunoModal').modal('show');
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}

function habilitaAluno(url) {
    var table = document.getElementById('tableAluno');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var aluno_id = this.cells[0].innerHTML;
            var aluno_ra = this.cells[1].innerHTML;
            var aluno_nome = this.cells[2].innerHTML;
            var aluno_telefone = this.cells[3].innerHTML;
            var aluno_aula = this.cells[4].innerHTML;
            var aluno_habilitado = this.cells[4].innerHTML;
            var data = {
                "aluno_id": aluno_id,
                "aluno_ra": aluno_ra,
                "aluno_nome": aluno_nome,
                "aluno_telefone": aluno_telefone,
                "aluno_aula": aluno_aula,
                "aluno_habilitado": aluno_habilitado,
            };
            var form = document.createElement('form');
            form.method = 'POST';
            form.target = '_self';
            form.action = url;
            for (var key in data) {
                var input = document.createElement('input');
                input.name = key;
                input.value = data[key];
                input.type = 'hidden';
                form.appendChild(input)
            }
            document.body.appendChild(form);
            form.submit();
        };
    }
}

function modalCadastrarAula() {
    jQuery.ajax({
        url: 'cadAula',
        type: 'POST',
        data: {
            "value": 'cadastrar',
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            document.getElementById('formModalAula').innerHTML = data;
            $('#cadastarAulaModal').modal('show');
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function modalUpdateAula(url) {
    var table = document.getElementById('tableAula');
    for (var i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            var aula_id = this.cells[0].innerHTML;
            var aula_iddia = this.cells[1].innerHTML;
            var aula_dia = this.cells[2].innerHTML;
            var aula_inicio = this.cells[3].innerHTML;
            var aula_fim = this.cells[4].innerHTML;
            var aula_iddisciplina = this.cells[5].innerHTML;
            var aula_disciplina = this.cells[6].innerHTML;
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    "aula_id": aula_id,
                    "aula_iddia": aula_iddia,
                    "aula_dia": aula_dia,
                    "aula_inicio": aula_inicio,
                    "aula_fim": aula_fim,
                    "aula_iddisciplina": aula_iddisciplina,
                    "aula_disciplina": aula_disciplina,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    document.getElementById('formModalAulaUp').innerHTML = data;
                    $('#updateAulaModal').modal('show');
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}

// function gerarQrCode(url) {
//     let table = document.getElementById('tableCode');
//     for (let i = 1; i < table.rows.length; i++) {
//         table.rows[i].onclick = function () {
//             let code_docente_id = this.cells[0].innerHTML;
//             let code_docente_idNivel = this.cells[1].innerHTML;
//             let code_docente_nome = this.cells[2].innerHTML;
//             let code_docente_email = this.cells[3].innerHTML;
//             let code_disciplina_nome = this.cells[4].innerHTML;
//             let code_disciplina_id = this.cells[5].innerHTML;
//             let code_dia_nome = this.cells[6].innerHTML;
//             let code_dia_id = this.cells[7].innerHTML;
//             let data = {
//                 "code_docente_id": code_docente_id,
//                 "code_docente_idNivel": code_docente_idNivel,
//                 "code_docente_nome": code_docente_nome,
//                 "code_docente_email": code_docente_email,
//                 "code_disciplina_nome": code_disciplina_nome,
//                 "code_disciplina_id": code_disciplina_id,
//                 "code_dia_nome": code_dia_nome,
//                 "code_dia_id": code_dia_id,
//             };
//             let form = document.createElement('form');
//             form.method = 'POST';
//             form.target = '_self';
//             form.action = url;
//             for (let key in data) {
//                 let input = document.createElement('input');
//                 input.name = key;
//                 input.value = data[key];
//                 input.type = 'hidden';
//                 form.appendChild(input)
//             }
//             document.body.appendChild(form);
//             form.submit();
//         };
//     }
// }

function gerarQrCode(url) {
    let table = document.getElementById('tableCode');
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            let data = new Date();
            let code_docente_id = this.cells[0].innerHTML;
            let code_docente_idNivel = this.cells[1].innerHTML;
            let code_docente_nome = this.cells[2].innerHTML;
            let code_docente_email = this.cells[3].innerHTML;
            let code_disciplina_nome = this.cells[4].innerHTML;
            let code_disciplina_id = this.cells[5].innerHTML;
            let code_dia_nome = this.cells[6].innerHTML;
            let code_dia_id = this.cells[7].innerHTML;
            let code_time = data.getHours() + data.getMinutes() + data.getSeconds();
            jQuery.ajax({
                url: url,
                type: 'POST',
                data: {
                    "code_docente_id": code_docente_id,
                    "code_docente_idNivel": code_docente_idNivel,
                    "code_docente_nome": code_docente_nome,
                    "code_docente_email": code_docente_email,
                    "code_disciplina_nome": code_disciplina_nome,
                    "code_disciplina_id": code_disciplina_id,
                    "code_dia_nome": code_dia_nome,
                    "code_dia_id": code_dia_id,
                    "code_time": code_time,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    document.getElementById('tituloCode').innerHTML = code_disciplina_nome + " - " + code_dia_nome;
                    document.getElementById('codigoQr').innerHTML = data;
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}
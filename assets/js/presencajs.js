//Submit form RNS

tempoP = Number();
tempoP = 30;

function startCountdownP() {
    if ((tempoP - 1) >= 0) {
        setTimeout('startCountdownP()', 1000);
        // console.log(tempoP);
        tempoP--;
    } else {
        tempoP = 30;
        load_presenca();
    }
    // setInterval(load_presenca(), 1 * 60000);
    load_presenca()
}

$("#data_presenca_id").datepicker({
    dateFormat: 'dd/mm/yy',
    dayNames: ['Domingo', 'Segunda', 'Terça', 'Quarta', 'Quinta', 'Sexta', 'Sábado'],
    dayNamesMin: ['D', 'S', 'T', 'Q', 'Q', 'S', 'S', 'D'],
    dayNamesShort: ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sáb', 'Dom'],
    monthNames: ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'],
    monthNamesShort: ['Jan', 'Fev', 'Mar', 'Abr', 'Mai', 'Jun', 'Jul', 'Ago', 'Set', 'Out', 'Nov', 'Dez'],
    nextText: 'Próximo',
    prevText: 'Anterior'
});

function load_presenca() {
    let disciplina = "";
    let dia = "";
    let mes = "";
    let ano = "";
    let dataTotal = "";
    try {
        let e = document.getElementById('disciplina_id');
        disciplina = e.options[e.selectedIndex].text;
        dia = $("#data_presenca_id").datepicker("getDate").getDate();
        mes = ($("#data_presenca_id").datepicker("getDate").getMonth() + 1);
        ano = $("#data_presenca_id").datepicker("getDate").getFullYear();
        dataTotal = $("#data_presenca_id").datepicker("getDate");
    } catch (e) {
        // console.log(e);
    }
    if (dataTotal == "") {
        let dNow = new Date();
        dataTotal = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();
    }
    jQuery.ajax({
        url: 'loadLista',
        type: 'POST',
        data: {
            "disciplina": disciplina,
            "dia": dia,
            "mes": mes,
            "ano": ano,
            "dataTotal": dataTotal,
        },
        dataType: 'html',
        success: function (data, textStatus, xhr) {
            $("#inserirLista").html(data);
        },
        error: function (xhr, textStatus, errorThrown) {
            console.log(textStatus.reponseText);
        }
    });
}

function post_presenca(tabela) {
    let disciplina = "";
    let dia = "";
    let mes = "";
    let ano = "";
    let dataTotal = "";
    try {
        let e = document.getElementById('disciplina_id');
        disciplina = e.options[e.selectedIndex].text;
        dia = $("#data_presenca_id").datepicker("getDate").getDate();
        mes = ($("#data_presenca_id").datepicker("getDate").getMonth() + 1);
        ano = $("#data_presenca_id").datepicker("getDate").getFullYear();
        dataTotal = $("#data_presenca_id").datepicker("getDate");
    } catch (e) {
        // console.log(e);
    }
    if (dataTotal == "") {
        let dNow = new Date();
        dataTotal = dNow.getDate() + '/' + (dNow.getMonth() + 1) + '/' + dNow.getFullYear();
    }
    let table = document.getElementById(tabela);
    for (let i = 1; i < table.rows.length; i++) {
        table.rows[i].onclick = function () {
            let aluno_id = this.cells[0].innerHTML;
            let aluno_ra = this.cells[1].innerHTML;
            let aluno_nome = this.cells[2].innerHTML;
            let presenca_check = document.getElementById(aluno_id).checked;
            // let presenca_check = this.cells[3].innerHTML;
            // alert("aluno_id... " + aluno_id + "\n"
            //     + "aluno_ra... " + aluno_ra + "\n"
            //     + "aluno_nome... " + aluno_nome + "\n"
            //     + "presenca_check... " + presenca_check);
            jQuery.ajax({
                url: 'loadLista',
                type: 'POST',
                data: {
                    "aluno_id": aluno_id,
                    "aluno_ra": aluno_ra,
                    "aluno_nome": aluno_nome,
                    "presenca_check": presenca_check,
                    "disciplina": disciplina,
                    "dia": dia,
                    "mes": mes,
                    "ano": ano,
                    "dataTotal": dataTotal,
                },
                dataType: 'html',
                success: function (data, textStatus, xhr) {
                    $("#inserirLista").html(data);
                },
                error: function (xhr, textStatus, errorThrown) {
                    console.log(textStatus.reponseText);
                }
            });
        };
    }
}

// setInterval(load_presenca(), 2 * 60000);
let url_atual = window.location.href;
if (url_atual.includes("presenca")) {
    startCountdownP();
}

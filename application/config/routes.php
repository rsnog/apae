<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
//$route['default_controller'] = 'welcome';
$route['default_controller'] = 'HomeController';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
$route['home'] = 'HomeController';
$route['contactform'] = 'HomeController/contactform';

$route['empresa'] = 'EmpresaController';

$route['api'] = 'User_api';
//$route['api?(:any)'] = 'ApiController';

$route['servico'] = 'ServicoController';
$route['logarS'] = 'ServicoController/logarForm';
$route['logarUser'] = 'ServicoController/logarUser';
$route['apk'] = 'ServicoController/apk';

$route['projeto'] = 'ProjetoController';
$route['contato'] = 'ContatoController';

$route['administra'] = 'AdmController';
$route['logoutClick'] = 'AdmController/logoutClick';
$route['qrcodeGenerator'] = 'AdmController/qrcodeGenerator';
$route['qrcodeGenerator/(:any)'] = 'AdmController/qrcodeGenerator';

$route['presenca'] = 'PresencaController';
$route['loadLista'] = 'PresencaController/loadLista';

$route['usuario'] = 'UsuarioController';
$route['cadUser'] = 'UsuarioController/cadastrarUserForm';
$route['cadUserDb'] = 'UsuarioController/cadastrarUser';
$route['upUser'] = 'UsuarioController/updateUserForm';
$route['upUserDb'] = 'UsuarioController/updateUser';
$route['hbUser'] = 'UsuarioController/habilitaUser';

$route['docente'] = 'DocenteController';

$route['disciplina'] = 'DisciplinaController';
$route['cadDiscip'] = 'DisciplinaController/cadastrarDiscipForm';
$route['cadDiscipDb'] = 'DisciplinaController/cadastrarDiscip';
$route['upDiscip'] = 'DisciplinaController/updateDiscipForm';
$route['upDiscipDb'] = 'DisciplinaController/updateDiscip';
$route['hbDiscip'] = 'DisciplinaController/habilitaDiscip';

$route['aluno'] = 'AlunoController';
$route['cadAluno'] = 'AlunoController/cadastrarAlunoForm';
$route['cadAlunoDb'] = 'AlunoController/cadastrarAluno';
$route['upAluno'] = 'AlunoController/updateAlunoForm';
$route['upAlunoDb'] = 'AlunoController/updateAluno';
$route['hbAluno'] = 'AlunoController/habilitaAluno';

$route['aula'] = 'AulaController';
$route['cadAula'] = 'AulaController/cadastrarAulaForm';
$route['cadAulaDb'] = 'AulaController/cadastrarAula';
$route['upAula'] = 'AulaController/updateAulaForm';
$route['upAulaDb'] = 'AulaController/updateAula';
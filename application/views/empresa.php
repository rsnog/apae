<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="http://www.nsystem.com.br/tcc/" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            Home</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url('index.php/empresa') ?>"
                                class="ttr_menu_items_parent_link_active">
                            EMPRESA
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/servico') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            SERVIÇO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/projeto') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            PROJETO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/contato') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            CONTATO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <div class="ttr_Firm-Profile_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">ALGUMAS PALAVRAS SOBRE NOSSA EMPRESA </span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Nossa Empresa nasceu da necessidade de alguns estudantes de "Ciência da Computação" criarem projetos que pudesse atender a escolas que necessitassem de soluções para tornar seus processos e operações automatizados. Com esse objetivo comum estes estudantes agora se empenham em oferecer um produto seguro e versátil baseado nas novas tecnologias com as quais se depararam durante seus anos de estudo. Somos motivados pela vontade de oferecer a nossos clientes o que precisam, muito antes que eles se deem conta do que necessitam.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

            <div class="ttr_Firm-Profile_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column20">
                        <div style="padding:20px;height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;line-height:3.52112676056338;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(0,58,0,40);">APRESENTAMOS UM OCEANO DE NOVAS OPORTUNIDADES</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

            <div style="" class="ttr_Firm-Profile_html_row3 row">
                <div class="post_column col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column30">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/73.png') ?>"
                                                style="max-width:120px;max-height:120px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">NOSSA MISSÃO</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Nossa missão é fazer sistemas acessíveis e amigáveis que qualquer pessoa no mundo possa utilizar para melhorar sua vida.</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <!--                                    <span><a HREF="#" target="_self"-->
                                <!--                                             class="btn btn-md btn-primary">mais detalhes</a></span>-->
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-xs-block">
                </div>
                <div class="post_column col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column31">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/74.png') ?>"
                                                style="max-width:120px;max-height:120px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">NOSSA VISÃO</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Para cumprir com esta missão, nos esforçamos constantemente em fornecer a nossos clientes soluções inovadoras, competitivas e sustentáveis, sempre levando em conta as necessidades do cliente.</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <!--                                    <span><a HREF="#" target="_self"-->
                                <!--                                             class="btn btn-md btn-primary">mais detalhes</a></span>-->
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-4 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column32">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/75.png') ?>"
                                                style="max-width:119px;max-height:119px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">NOSSOS VALORES</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Temos como valores a Colaboração - dar e receber ajuda, a Excelência - encontrar as melhores soluções, a Agilidade - pensar de forma abrangente e agir com prontidão e o Respeito - ouvir e compreender, sermos francos.</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <!--                                    <span><a HREF="#" target="_self"-->
                                <!--                                             class="btn btn-md btn-primary">mais detalhes</a></span>-->
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-xs-block">
                </div>
            </div>

            <div class="ttr_Firm-Profile_html_row4 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column40">
                        <div style="padding-top:40px;height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">NOSSO TIME</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column41">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/76.jpg') ?>"
                                                style="max-width:250px;max-height:250px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(1,156,204,1);">André C. Torres</span>
                            </p>
                            <p style="text-align:Center;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(34,34,34,1);">(Gerente Geral)</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-xs-block">
                </div>
                <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column42">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/77.jpg') ?>"
                                                style="max-width:250px;max-height:250px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(1,156,204,1);">Roberto N. Souza</span>
                            </p>
                            <p style="margin:0.36em 0em 0.36em 0em;text-align:Center;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(34,34,34,1);">(Estagiário)</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column43">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/78.jpg') ?>"
                                                style="max-width:250px;max-height:249px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(1,156,204,1);">Severiano C. Neto</span>
                            </p>
                            <p style="margin:0.36em 0em 0.36em 0em;text-align:Center;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(34,34,34,1);">(Diretor de Criação)</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-xs-block">
                </div>

                <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column44">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span class="ttr_image"
                                                                                      style="float:none;display:block;text-align:center;overflow:hidden;margin:0em 0em 2.14em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/79.jpg') ?>"
                                                style="max-width:250px;max-height:250px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(1,156,204,1);">Tonys C.B. Soares</span>
                            </p>
                            <p style="margin:0.36em 0em 0.36em 0em;text-align:Center;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(34,34,34,1);">(Gerente de RH)</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

            <div class="ttr_Firm-Profile_html_row5 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Firm-Profile_html_column50">
                        <div style="padding-top:40px;height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RELATOS DE NOSOS CLIENTES</span>
                            </p>
                            <p style="margin:2.14em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">...... </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:2.11267605633803;">
                                <span style="font-family:'Roboto','Arial';font-weight:500;font-size:1.429em;color:rgba(34,34,34,1);">Marco A. Miquelino</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
        </div>
    </div>
    <div style="clear:both">
    </div>
</div>
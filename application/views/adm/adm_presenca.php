<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/administra") ?>"
                                class="ttr_menu_items_parent_link">
                            CHAMADA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url("index.php/presenca") ?>"
                                class="ttr_menu_items_parent_link_active">
                            PRESENÇA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/usuario") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            USUÁRIOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/docente") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            DOCENTES
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/disciplina") ?>"
                                class="ttr_menu_items_parent_link">
                            DISCIPLINAS
                        </a>
                        <hr class="horiz_separator"/>
                        <!--                            <ul class="child dropdown-menu" role="menu">-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aluno") ?><!--">Alunos</a></li>-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aula") ?><!--">Aulas</a></li>-->
                        <!--                            </ul>-->
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aluno") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            ALUNOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aula") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            AULAS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>
<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <div class="ttr_Services_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Services_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center; margin-bottom:20px;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">QUADRO DE PRESENÇA</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Services_html_column01">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span class="ttr_image"
                                                           style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/aula.png') ?>"
                                                style="max-width:119px;max-height:119px;"/></span></span>
                                <span class="center-block"
                                      style="text-align:center; font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">ALUNO / AULAS</span>
                            </p>
                            <br>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Verificar ou alterar presença.</span>
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;">
                            <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="ttr_Projects_html_column23">
                                    <div class="html_content">
                                        <p style="margin-top:20px;padding:0;line-height:1.97183098591549;">
                                            <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">
                                                <?php echo form_label('Disciplina : ', 'disciplina'); ?>
                                            </span>
                                        </p>
                                        <p style="margin:0;padding:0;line-height:1.54929577464789;">
                                            <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">
                                            <?php
                                            $options = array('' => 'Selecione...');
                                            foreach ($disciplinaDados as $d) {
                                                $options[$d->disciplina_id] = $d->disciplina_nome;
                                            }
                                            echo form_dropdown('', $options, set_value('disciplina', ''), array('id' => 'disciplina_id', 'class' => 'msg-box', 'onchange' => 'load_presenca()', 'style' => 'width: 90%'));
                                            ?>
                                        </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                <div class="ttr_Projects_html_column24">
                                    <div class="html_content">
                                        <p style="margin-top:20px; margin-left: 30px;line-height:1.97183098591549;">
                                            <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em">
                                                <?php echo form_label('Dia : ', 'dia'); ?>
                                            </span>
                                        <div style="clear:both" id="diaSemana" style="background-color: red">
                                        </p>
                                        <p style="margin-top:0px; margin-left: 30px;line-height:1.54929577464789">
                                            <span style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em">
                                               <?php
                                               $data_presenca = array(
                                                   'name' => 'data_presenca',
                                                   'id' => 'data_presenca_id',
                                                   'class' => 'form-control input_box',
                                                   'placeholder' => 'Insira uma data',
                                                   'style' => 'width: 45%',
                                                   'onchange' => 'load_presenca()'
                                               );
                                               echo form_input($data_presenca);
                                               ?>
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix visible-xs-block"></div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
        </div>
    </div>
    <div style="clear:both" id="inserirLista" style="background-color: red">
    </div>
</div>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/administra") ?>"
                                class="ttr_menu_items_parent_link">
                            CHAMADA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/presenca") ?>"
                                class="ttr_menu_items_parent_link">
                            PRESENÇA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/usuario") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            USUÁRIOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url("index.php/docente") ?>"
                                class="ttr_menu_items_parent_link_active">
                            DOCENTES
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/disciplina") ?>"
                                class="ttr_menu_items_parent_link">
                            DISCIPLINAS
                        </a>
                        <hr class="horiz_separator"/>
                        <!--                            <ul class="child dropdown-menu" role="menu">-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aluno") ?><!--">Alunos</a></li>-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aula") ?><!--">Aulas</a></li>-->
                        <!--                            </ul>-->
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aluno") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            ALUNOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aula") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            AULAS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <!--                    <li class="ttr_menu_items_parent dropdown">-->
                    <!--                        <a onclick="logoutClicado()" href="-->
                    <?php //echo base_url('') ?><!--"-->
                    <!--                           class="ttr_menu_items_parent_link">-->
                    <!--                            LOGOUT-->
                    <!--                        </a>-->
                    <!--                        <hr class="horiz_separator"/>-->
                    <!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">

            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>

            <div class="ttr_Home_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Home_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">LISTA DE DOCENTES</span>
                            <div class="table-responsive text-nowrap">
                                <table id="tableUsuario" class="table table-striped table-advance table-hover w-auto">
                                    <thead>
                                    <tr>
                                        <th class="hidden">Id</th>
                                        <th class="hidden">Id_Nivel</th>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Disciplina</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($docenteDados as $t) {
                                        echo "<tr>" .
                                            "<td class=\"hidden\">" . $t->usuario_id . "</td>" .
                                            "<td class=\"hidden\">" . $t->id_niveis . "</td>" .
                                            "<td>" . $t->usuario_nome . "</td>" .
                                            "<td>" . $t->usuario_email . "</td>" .
                                            "<td>" . $t->disciplina_nome . "</td>" .
                                            "</tr> ";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

        </div>
    </div>
</div>

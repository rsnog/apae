<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url("index.php/administra") ?>"
                                class="ttr_menu_items_parent_link_active">
                            CHAMADA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/presenca") ?>"
                                class="ttr_menu_items_parent_link">
                            PRESENÇA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/usuario") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            USUÁRIOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/docente") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            DOCENTES
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/disciplina") ?>"
                                class="ttr_menu_items_parent_link">
                            DISCIPLINAS
                        </a>
                        <hr class="horiz_separator"/>
                        <!--                            <ul class="child dropdown-menu" role="menu">-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aluno") ?><!--">Alunos</a></li>-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aula") ?><!--">Aulas</a></li>-->
                        <!--                            </ul>-->
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aluno") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            ALUNOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aula") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            AULAS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <!--                    <li class="ttr_menu_items_parent dropdown">-->
                    <!--                        <a onclick="logoutClicado()" href="-->
                    <?php //echo base_url('') ?><!--"-->
                    <!--                           class="ttr_menu_items_parent_link">-->
                    <!--                            LOGOUT-->
                    <!--                        </a>-->
                    <!--                        <hr class="horiz_separator"/>-->
                    <!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <div class="ttr_Services_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Services_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center; margin-bottom:20px;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">GERAR QRCODE PARA CHAMADA</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Services_html_column01">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span class="ttr_image"
                                                           style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/80.png') ?>"
                                                style="max-width:119px;max-height:119px;"/></span></span>
                                <span class="center-block"
                                      style="text-align:center; font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">DOCENTE / AULAS</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Selecione o docente e sua respectiva aula para gerar o código de chamada.</span>
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;">

                            <div class="table-responsive text-nowrap">
                                <table id="tableCode" class="table table-striped table-advance table-hover w-auto">
                                    <thead>
                                    <tr>
                                        <th class="hidden">Id</th>
                                        <th class="hidden">Id_Nivel</th>
                                        <th>Nome</th>
                                        <th class="hidden">E-mail</th>
                                        <th>Disciplina</th>
                                        <th class="hidden">DisciplinaId</th>
                                        <th>Dia</th>
                                        <th class="hidden">DiaId</th>
                                        <th class='centered <?php if ($_SESSION['usuario_perfil'] == 3): echo('hidden'); endif; ?>'>
                                            QrCode
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($docenteDados as $t) {
                                        echo "<tr>" .
                                            "<td class=\"hidden\">" . $t->usuario_id . "</td>" .
                                            "<td class=\"hidden\">" . $t->id_niveis . "</td>" .
                                            "<td>" . $t->usuario_nome . "</td>" .
                                            "<td class=\"hidden\">" . $t->usuario_email . "</td>" .
                                            "<td>" . $t->disciplina_nome . "</td>" .
                                            "<td class=\"hidden\">" . $t->disciplina_id . "</td>" .
                                            "<td>" . $t->dia_nome . "</td>" .
                                            "<td class=\"hidden\">" . $t->dia_id . "</td>" .
                                            "<td class='centered'>" . "<a>
                                      <button id='buttonQrCode' data-toggle=\"tooltip\" title=\"Gerar código de chamada\" onclick=\"gerarQrCode('qrcodeGenerator');\" class=\"btn btn-primary btn-xs\"><i class=\"fas fa-qrcode\"></i></button></a>"
                                            . "</td > " .
                                            "</tr> ";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="clearfix visible-xs-block"></div>

                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Services_html_column02">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="align-content: center" class="html_content">
                            <p><span class="ttr_image"
                                     style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                class="ttr_uniform" src=""
                                                style="max-width:120px;max-height:120px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);"> </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;margin-bottom: 10px;">
                            <span class="center-block"
                                  style="text-align:center; font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">
                                    <span id="tituloCode" class="center-block" style="text-align:center;">
                                    <?php
                                    $kode = 'Autenticação de Presença...';
                                    echo $kode;
                                    ?>
                                    </span>
                                <div class="hidden"
                                     style="background-color: black; padding: 5px; width: fit-content; height: fit-content">
                                        <img src="<?php echo site_url('AdmController/qrcodeGenerator/' . $kode) ?>"
                                             alt="QrCode">
                                </div>
                                <div id="codigoChamada" class="center-block"
                                     style="margin-bottom: 20px; background-color: black; padding: 15px; width: fit-content; height: fit-content">

                                    <div id="codigoQr">
                                         <img id="codigoChamada2" src=' <?php echo base_url() . 'tes1.png' ?> '/>';
                                    </div>

                                    <!--                                    --><?php
                                    //                                    echo '<img id="codigoChamada2" src="' . base_url('/assets/qr_code/') . 'tes1.png" />';
                                    //                                    ?>

                                </div>
                                </span>
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

            </div>
            <div style="clear:both"></div>
        </div>
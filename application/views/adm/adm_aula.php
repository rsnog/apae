<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/administra") ?>"
                                class="ttr_menu_items_parent_link">
                            CHAMADA</a>
                        <hr class="horiz_separator"/>
                        <!--                        <ul class="child dropdown-menu" role="menu">-->
                        <!--                            <li><a href="-->
                        <!--                        -->
                        <?php //echo base_url("index.php/presenca") ?><!--">Presença</a></li>-->
                        <!--                        </ul>-->
                        <!--                        <hr class="horiz_separator"/>-->
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/presenca") ?>"
                                class="ttr_menu_items_parent_link">
                            PRESENÇA</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/usuario") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            USUÁRIOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/docente") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            DOCENTES
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a
                                href="<?php echo base_url("index.php/disciplina") ?>"
                                class="ttr_menu_items_parent_link">
                            DISCIPLINAS
                        </a>
                        <hr class="horiz_separator"/>
                        <!--                            <ul class="child dropdown-menu" role="menu">-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aluno") ?><!--">Alunos</a></li>-->
                        <!--                                <li><a href="-->
                        <?php //echo base_url("index.php/aula") ?><!--">Aulas</a></li>-->
                        <!--                            </ul>-->
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url("index.php/aluno") ?>"
                                                                  class="ttr_menu_items_parent_link">
                            ALUNOS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a href="<?php echo base_url("index.php/aula") ?>"
                                                                         class="ttr_menu_items_parent_link_active">
                            AULAS
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <!--                    <li class="ttr_menu_items_parent dropdown">-->
                    <!--                        <a onclick="logoutClicado()" href="-->
                    <?php //echo base_url('') ?><!--"-->
                    <!--                           class="ttr_menu_items_parent_link">-->
                    <!--                            LOGOUT-->
                    <!--                        </a>-->
                    <!--                        <hr class="horiz_separator"/>-->
                    <!--                    </li>-->
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                    <span>
                                        <?php
                                        if ($msg = get_msg()):
                                            echo '<div style="width:fit-content; margin-left: 39%; float: left" class="msg-box">' . $msg . '</div>';
                                        endif;
                                        ?>
                                    </span>
            </p>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>

            <div class="ttr_Home_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Home_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">LISTA DE AULAS</span>

                            <h3 <?php if ($_SESSION['usuario_perfil'] == 3): echo("hidden"); endif; ?> ><a>
                                    <button onclick="modalCadastrarAula();" type="button"
                                            class="btn btn-success btn-lg"><i class="fa fa-user-o"></i>
                                        Cadastrar nova aula
                                    </button>
                                </a></h3>

                            <div class="table-responsive text-nowrap">
                                <table id="tableAula" class="table table-striped table-advance table-hover w-auto">
                                    <thead>
                                    <tr>
                                        <th class="hidden">Id</th>
                                        <th class="hidden">IdDia</th>
                                        <th>Dia</th>
                                        <th>Início</th>
                                        <th>Fim</th>
                                        <th class="hidden">IdDisciplina</th>
                                        <th>Disciplina</th>

                                        <?php
                                        if ($_SESSION['usuario_perfil'] != 3):
                                            echo "<th class='centered'>
                                            Editar
                                        </th>";
                                        endif;
                                        ?>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    foreach ($aulaDados as $t) {
                                        if ($_SESSION['usuario_perfil'] == 3):
//                                            if ($t->usuario_id == $_SESSION['usuario_id']):
                                            echo "<tr>" .
                                                "<td class=\"hidden\">" . $t->aula_id . "</td>" .
                                                "<td class=\"hidden\">" . $t->dia_id . "</td>" .
                                                "<td>" . $t->dia_nome . "</td>" .
                                                "<td>" . $t->hora_inicio . "</td>" .
                                                "<td>" . $t->hora_fim . "</td>" .
                                                "<td class=\"hidden\">" . $t->disciplina_id . "</td>" .
                                                "<td>" . $t->disciplina_nome . "</td>" .
                                                "<td>";
//                                                ."<a>
//                                <button id='buttonAltera' data-toggle=\"tooltip\" title=\"Detalhes\" onclick=\"modalUpdateAula('upAula');\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-pencil-square-o\"></i></button>
//                                </a>" . "</td > ";
//                                            endif;
//                                            continue;
                                        else:
                                            echo "<tr>" .
                                                "<td class=\"hidden\">" . $t->aula_id . "</td>" .
                                                "<td class=\"hidden\">" . $t->dia_id . "</td>" .
                                                "<td>" . $t->dia_nome . "</td>" .
                                                "<td>" . $t->hora_inicio . "</td>" .
                                                "<td>" . $t->hora_fim . "</td>" .
                                                "<td class=\"hidden\">" . $t->disciplina_id . "</td>" .
                                                "<td>" . $t->disciplina_nome . "</td>" .
                                                "<td>" . "<a>
                                <button id='buttonAltera' data-toggle=\"tooltip\" title=\"Detalhes\" onclick=\"modalUpdateAula('upAula');\" class=\"btn btn-primary btn-xs\"><i class=\"fa fa-pencil-square-o\"></i></button>
                                </a>" . "</td > ";
                                        endif;

                                        echo "</tr> ";
                                    }
                                    ?>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

        </div>
    </div>
</div>

<!-- Modal cadastrar aula-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="cadastarAulaModal"
     class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img style="margin-left:10px;margin-top: 1px;padding-top: 1px" class="ttr_menu_logo"
                     src="<?php echo base_url("assets/img/menulogo.png") ?>">
                <h4 style="margin-top:45px; text-align: center; font-size: x-large" class="modal-title">
                    Cadastrar Aula</h4>
            </div>
            <div id="formModalAula" class="modal-body"><!-- AQUI O ID --></div>
        </div>
    </div>
</div>
<!-- modal -->

<!-- Modal alterar aula-->
<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="updateAulaModal"
     class="modal fade" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <img style="margin-left:10px;margin-top: 1px;padding-top: 1px" class="ttr_menu_logo"
                     src="<?php echo base_url("assets/img/menulogo.png") ?>">
                <h4 style="margin-top:45px; text-align: center; font-size: x-large" class="modal-title">
                    Alterar Aula</h4>
            </div>
            <div id="formModalAulaUp" class="modal-body"><!-- AQUI O ID --></div>
        </div>
    </div>
</div>
<!-- modal -->

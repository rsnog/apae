<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>
        <?php echo "Autenticação de Presença - " . $titulo ?>
    </title>

    <!-- Bootstrap core CSS -->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo base_url("assets/lib/bootstrap/css/bootstrap.min.css") ?><!--">-->
    <!--external css-->
    <!--    <link rel="stylesheet" href="-->
    <?php //echo base_url("assets/lib/font-awesome/css/font-awesome.css") ?><!--">-->
    <!--    <link rel="stylesheet" type="text/css" href="-->
    <?php //echo base_url("assets/lib/gritter/css/jquery.gritter.css") ?><!--">-->

    <!-- Custom styles for this template -->
    <!--    <script src="--><?php //echo base_url('/assets/lib/chart-master/Chart.js'); ?><!--"></script>-->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!--    <link rel="stylesheet" href="//resources/demos/style.css">-->

    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <!--    <script type="text/javascript" src="--><?php //echo base_url('assets/js/jquery.js') ?><!--">-->
    <!--    </script>-->
    <!--    <script type="text/javascript" src="--><?php //echo base_url('assets/js/jquery-ui.min.js') ?><!--">-->
    <!--    </script>-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/tt_slideshow.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/Customjs.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/contactform.js') ?>"></script>

    <!--    </script>-->
    <!--    <script type="text/javascript" src="--><?php //echo base_url('assets/js/tempoLogin.js') ?><!--">-->
    <!--    </script>-->
    <script src="https://kit.fontawesome.com/7b6552584b.js"></script>


    <link rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.css') ?>" type="text/css" media="screen"/>
    <link rel="stylesheet" href="<?php echo base_url('assets/css/style.css') ?>" type="text/css" media="screen"/>
    <!--    <!--[if lte IE 8]>-->
    <!--    <link rel="stylesheet" href="menuie.css" type="text/css" media="screen"/>-->
    <!--    <link rel="stylesheet" href="vmenuie.css" type="text/css" media="screen"/>-->
    <!-- < ![endif]-->
    <script type="text/javascript" src="<?php echo base_url('assets/js/totop.js') ?>"></script>

    <!--[if IE 7]>
    <style type="text/css" media="screen">
        #ttr_vmenu_items li.ttr_vmenu_items_parent {
            margin-left: -16px;
            font-size: 0px;
        }
    </style>
    <![endif]-->
    <!--[if lt IE 9]>
    <script type="text/javascript" src="<?php echo base_url('assets/js/html5shiv.js') ?>"></script>
    <script type="text/javascript" src="<?php echo base_url('assets/js/respond.min.js') ?>"></script>
    <![endif]-->


</head>
<body class="Home">
<div class="totopshow">
    <a href="#" class="back-to-top"><img alt="Voltar para o Topo"
                                         src="<?php echo base_url('assets/img/gototop0.png') ?>"/></a>
</div>
<div id="ttr_page" class="container">
    <header id="ttr_header">
        <div id="ttr_header_inner">
            <div class="ttr_headershape01">
                <div class="html_content"><p style="margin:0em 0em 0em 0em;line-height: normal;"><br
                                style="font-size:0.571em;"/></p>
                    <p style="margin:0em 0em 0em 0em;text-align:Center;line-height: normal;">
                        <span style="font-family:'Roboto','Arial';font-weight:300;color:rgba(153,153,153,1);">
                            <a onclick="logoutClicado()" href="<?php echo base_url('') ?>"
                               class="ttr_menu_items_parent_link"><?php
                                if (isset($nomeLogged)):
                                    echo $nomeLogged . " - LOGOUT";
                                else:
                                    echo('Jaguariúna - SP');
                                endif;
                                ?></a>
                        </span>
                    </p></div>
            </div>
            <div class="ttr_headershape02">
                <div class="html_content"><p style="margin:0em 0em 0em 0em;text-align:Center;line-height: normal;"><br
                                style="font-size:0.571em;"/></p>
                    <p style="margin:0em 0em 0em 0em;text-align:Center;line-height: normal;">
                        <span ID="sessaoT"
                              style="font-family:'Roboto','Arial';font-weight:300;color:rgba(153,153,153,1);">

                        </span>
                    </p></div>
            </div>
        </div>
    </header>
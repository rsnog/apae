<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="http://www.nsystem.com.br/tcc/" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            Home</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/empresa') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            EMPRESA
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url('index.php/servico') ?>"
                                class="ttr_menu_items_parent_link_active">
                            SERVIÇO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/projeto') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            PROJETO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/contato') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            CONTATO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <div class="ttr_Services_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Services_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">O QUE NÓS FAZEMOS</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Services_html_column01">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span class="ttr_image"
                                                           style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/80.png') ?>"
                                                style="max-width:119px;max-height:119px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">AUTENTICAÇÃO DE PRESENÇA PARA AMBIENTES ESCOLARES</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Possibilitamos que o processo da autenticação da chamada em sala de aula seja feito de forma automática e segura para que partir dele pudessem ser emitidos relatórios destas chamadas. Um aplicativo para telefone Android fica responsável pela autenticação do usuário através da utilização da câmera do aparelho para a leitura do QRcode gerado pelo Sistema e os dados são enviados para o servidor, que registra a presença; </span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-xs-block"></div>

                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Services_html_column02">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span class="ttr_image"
                                                           style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                class="ttr_uniform" src="<?php echo base_url('assets/img/apk.png') ?>"
                                                style="max-width:120px;max-height:120px;"/></span></span><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">Download do aplicativo </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Façao download do aplicativo para efetivar a sua presença através do celular. </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                    <span>
                                        <a href="<?php echo base_url('index.php/apk') ?>" style="margin-top:50%">
                                            <button id='buttonLogar' data-toggle="tooltip" title="Baixar arquivo"
                                                    onclick=""
                                                    class="btn btn-md btn-primary">Download</button>
                                        </a>
                                    </span>
                            </p>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="ttr_Services_html_row1 row">
                    <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="ttr_Services_html_column03">
                            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                            <div class="html_content"><p>&nbsp;</p>
                                <p style="margin:0.14em 0em 0em 0.57em;text-align:Center;"><span
                                            style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">ACESSO AO SISTEMA</span>
                                </p></div>
                            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                            <div style="clear:both;"></div>
                        </div>
                    </div>
                    <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                    </div>

                    <div style="margin-bottom: 30px" class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="ttr_Services_html_column04">
                            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                            <div class="html_content"><p><span class="ttr_image"
                                                               style="float:Left;overflow:hidden;margin:0em 1.43em 0em 0em;"><span><img
                                                    class="ttr_uniform"
                                                    src="<?php echo base_url('assets/img/86.jpg') ?>"
                                                    style="max-width:350px;max-height:233px;"/></span></span><span
                                            style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(34,34,34,1);">Sistema de Cadastro, Gerenciamento e Visualização das Informações</span>
                                </p>
                                <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                            style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(105,105,105,1);">Através da utilização do nosso Sistema On-line é possível o cadastro, gerenciamento e visualização do corpo docente, das aulas, dos alunos e de seus registros de presença realizados através de seus smartphones. Se ainda não é cliente entre em contato para mais informações. </span>
                                </p>

                                <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                    <span>
                                        <a style="margin-top:50%">
                                            <button id='buttonLogar' data-toggle="tooltip" title="Entrar no Sistema"
                                                    onclick="modalLogarS();"
                                                    class="btn btn-md btn-primary">Logar</button>
                                        </a>
                                    </span>
                                </p>

                                <p style="margin:0.71em 0em 0.36em 0em;text-align:Center;line-height:1.69014084507042;">
                                    <span>
                                        <?php
                                        if ($msg = get_msg()):
                                            echo '<div style="width:fit-content; margin-right: 26%; float: right;" class="msg-box">' . $msg . '</div>';
                                        endif;
                                        ?>
                                    </span>
                                </p>

                            </div>

                            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                            <div style="clear:both;"></div>
                        </div>

                        <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                        </div>
                    </div>

                    <div class="clearfix visible-xs-block">
                    </div>

                    <div class="clearfix visible-xs-block">
                    </div>

                    <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                </div>
            </div>
            <div style="clear:both"></div>
        </div>

        <!-- Modal logar-->
        <div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="logarModal"
             class="modal fade" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <img style="margin-left:10px;margin-top: 1px;padding-top: 1px" class="ttr_menu_logo"
                             src="<?php echo base_url("assets/img/menulogo.png") ?>">
                        <h4 style="margin-top:30px; text-align: center; font-size: x-large" class="modal-title">
                            Login</h4>
                    </div>
                    <div id="formLogar" class="modal-body"><!-- AQUI O ID --></div>
                </div>
            </div>
        </div>
        <!-- modal -->
<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>

<div style="height:0px;width:0px;overflow:hidden;"></div>
<footer id="ttr_footer">
    <div class="ttr_footerHome_html_row0 row">
        <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <div class="ttr_footerHome_html_column00">
                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                <!--                <div class="html_content"><p style="margin:0.36em 0em 0.36em 2.86em;">&nbsp;</p>-->
                <!--                    <p style="margin:0.36em 0em 0.36em 2.86em;"><span-->
                <!--                                style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(0,182,240,1);">NAVIGATION</span>-->
                <!--                    </p>-->
                <!--                    <p style="margin:1.43em 0em 0.36em 2.86em;"><a HREF="Home.html" class="tt_link"-->
                <!--                                                                   target="_self"><span-->
                <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Home</span></a>-->
                <!--                    </p>-->
                <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><a HREF="Firm-Profile.html" class="tt_link"-->
                <!--                                                                   target="_self"><span-->
                <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Firm Profile&nbsp;&nbsp;</span></a>-->
                <!--                    </p>-->
                <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><a HREF="Services.html" class="tt_link"-->
                <!--                                                                   target="_self"><span-->
                <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Services</span></a>-->
                <!--                    </p>-->
                <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><a HREF="Events.html" class="tt_link"-->
                <!--                                                                   target="_self"><span-->
                <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Events</span></a>-->
                <!--                    </p>-->
                <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><a HREF="Contact.html" class="tt_link"-->
                <!--                                                                   target="_self"><span-->
                <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Contact</span></a>-->
                <!--                    </p></div>-->
                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            </div>
        </div>
        <div class="clearfix visible-xs-block">
        </div>
        <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <!--            <div class="ttr_footerHome_html_column01">-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--                <div class="html_content"><p style="margin:0.36em 0em 0.36em 2.86em;">&nbsp;</p>-->
            <!--                    <p style="margin:0.36em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(0,182,240,1);">SERVICES</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:1.43em 0em 0em 2.86em;"><a HREF="Services.html" class="tt_link"-->
            <!--                                                                target="_self"><span-->
            <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Client Searvices</span></a>&nbsp;&nbsp;-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0em 2.86em;font-family:'<String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;>&amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;gt;&amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;gt;&amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;gt;&amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;amp;gt;Arial&amp;amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/String&amp;amp;amp;gt;&amp;amp;lt;/String&amp;amp;gt;&amp;lt;/String&amp;gt;</String>';font-size:1.333em;">-->
            <!--                        <a HREF="Services.html" class="tt_link" target="_self"><span-->
            <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Economic Consulting </span></a>&nbsp;&nbsp;-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0em 2.86em;font-family:'<String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;>&amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;gt;&amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;gt;&amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;gt;&amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;amp;gt;Arial&amp;amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/String&amp;amp;amp;gt;&amp;amp;lt;/String&amp;amp;gt;&amp;lt;/String&amp;gt;</String>';font-size:1.333em;">-->
            <!--                        <a HREF="Services.html" class="tt_link" target="_self"><span-->
            <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Strategic Communications </span></a>&nbsp;&nbsp;-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0em 2.86em;font-family:'<String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;>&amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;gt;&amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;gt;&amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;gt;&amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;amp;lt;String xmlns=&quot;clr-namespace:System;assembly=mscorlib&quot;&amp;amp;amp;amp;amp;amp;amp;amp;gt;Arial&amp;amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;amp;gt;&amp;amp;amp;amp;lt;/String&amp;amp;amp;amp;gt;&amp;amp;amp;lt;/String&amp;amp;amp;gt;&amp;amp;lt;/String&amp;amp;gt;&amp;lt;/String&amp;gt;</String>';font-size:1.333em;">-->
            <!--                        <a HREF="Services.html" class="tt_link" target="_self"><span-->
            <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Technology Consulting</span></a>&nbsp;&nbsp;-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0em 2.86em;"><a HREF="Services.html" class="tt_link"-->
            <!--                                                                target="_self"><span-->
            <!--                                    style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Executive search</span></a>&nbsp;&nbsp;-->
            <!--                    </p></div>-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--            </div>-->
        </div>
        <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
        </div>
        <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <!--            <div class="ttr_footerHome_html_column02">-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--                <div class="html_content"><p style="margin:0.36em 0em 0.36em 2.86em;">&nbsp;</p>-->
            <!--                    <p style="margin:0.36em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(0,182,240,1);">ARCHIVES</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:1.43em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">July 2014</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Sep 2014</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Dec 2014</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">March 2015</span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">June 2015</span>-->
            <!--                    </p></div>-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--            </div>-->
        </div>
        <div class="clearfix visible-xs-block">
        </div>
        <div class="post_column col-lg-3 col-md-6 col-sm-6 col-xs-12">
            <!--            <div class="ttr_footerHome_html_column03">-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--                <div class="html_content"><p style="margin:0.36em 0em 0.36em 2.86em;">&nbsp;</p>-->
            <!--                    <p style="margin:0.36em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;color:rgba(0,182,240,1);">RECENT POSTS </span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:1.43em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Arenean Nonummy </span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Hendrerit Mauselntes </span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Ulusm Duifusceras </span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Vitaeneque Turpisin </span>-->
            <!--                    </p>-->
            <!--                    <p style="margin:0.57em 0em 0.36em 2.86em;"><span-->
            <!--                                style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(255,255,255,1);">Praesent Pulvinar </span>-->
            <!--                    </p></div>-->
            <!--                <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
            <!--            </div>-->
        </div>
        <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
        </div>
    </div>
    <div class="ttr_footer_bottom_footer">
        <div class="ttr_footer_bottom_footer_inner">
            <div class="ttr_footershape1">
                <div class="html_content"><p>&nbsp;</p></div>
            </div>
            <div class="ttr_footershape2">
                <div class="html_content"><p>&nbsp;</p></div>
            </div>
            <div id="ttr_copyright">
                <a href="#">
                    Autenticação de Presença © 2019 Todos os direitos reservados
                </a>
            </div>
            <a href="http://www.facebook.com/" class="ttr_footer_facebook" target="_blank">
            </a>
            <a href="http://twitter.com/" class="ttr_footer_twitter" target="_blank">
            </a>
            <a href="http://www.nsystem.com.br/tcc" class="ttr_footer_googleplus" target="_blank">
            </a>
        </div>
    </div>
</footer>
<div style="height:0px;width:0px;overflow:hidden;-webkit-margin-bottom-collapse: separate;"></div>
</div>

<!-- js placed at the end of the document so the pages load faster -->
<!--<script src="--><?php //echo base_url('/assets/lib/bootstrap/js/bootstrap.min.js'); ?><!--"></script>-->
<!--<script class="include" type="text/javascript" src="--><?php //echo base_url('/assets/lib/jquery.dcjqaccordion.2.7.js');?><!--"></script>-->
<!--<script src="--><?php //echo base_url('/assets/lib/jquery.scrollTo.min.js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url('/assets/lib/jquery.nicescroll.js'); ?><!--"></script>-->
<!--common script for all pages-->
<!--<script src="--><?php //echo base_url('/assets/lib/common-scripts.js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url('/assets/lib/gritter/js/jquery.gritter.js'); ?><!--"></script>-->
<!--<script type="text/javascript" src="--><?php //echo base_url('/assets/lib/gritter-conf.js'); ?><!--"></script>-->
<!--script for this page-->
<!--<script src="--><?php //echo base_url('assets/js/presencajs.js') ?><!--"></script>-->
<script src="<?php echo base_url('assets/js/apaetcc.js') ?>"></script>
<!--<script src="--><?php //echo base_url('/assets/js/tempoLogin.js'); ?><!--"></script>-->

<script type="text/javascript">
    WebFontConfig = {
        google: {families: ['Roboto+Slab:700', 'Roboto+Slab']}
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.0.31/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>
</body>
</html>

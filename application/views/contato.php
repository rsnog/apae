<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="http://www.nsystem.com.br/tcc/" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            Home</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/empresa') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            EMPRESA
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/servico') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            SERVIÇO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/projeto') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            PROJETO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url('index.php/contato') ?>"
                                class="ttr_menu_items_parent_link_active">
                            CONTATO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <div class="ttr_Contact_html_row0 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Contact_html_column00">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">ENTRE EM CONTATO</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Contact_html_column01">
                        <div style="margin:10px;padding:10px;height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.714em;color:rgba(34,34,34,1);">Formulário de Contato</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;">
                            <form id="ContactForm2" class="form-horizontal" role="form" method="post" action=""
                                  style="padding:0px 0px 0px 0px;float:left;">
                                <div class="form-group"><label class="col-sm-4 control-label">Nome</label>
                                    <div class="col-sm-8"><input type="text" class="form-control required nomesend"
                                                                 data-vali="novalidation"/></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label required">E-mail</label>
                                    <div class="col-sm-8"><input type="text" class="form-control required"
                                                                 data-vali="email"/></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label">Assunto</label>
                                    <div class="col-sm-8"><input type="text" class="form-control required assunto"
                                                                 data-vali="novalidation"/></div>
                                </div>
                                <div class="form-group"><label class="col-sm-4 control-label">Mensagem</label>
                                    <div class="col-sm-8"><textarea class="form-control required comment" name="message"
                                                                    rows="4"></textarea></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-8"><input type="submit"
                                                                                 class="pull-right btn btn-sm btn-primary"
                                                                                 rows="4" id="submit" name="submit"
                                                                                 value="Enviar mensagem"/></div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="success"></div>
                                <div class="req_field"></div>
                                <div class="clearfix"></div>
                            </form>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="ttr_Contact_html_column02">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.714em;color:rgba(34,34,34,1);">Informações de Contato</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;color:rgba(34,34,34,1);">Utilize os campos do formulário para poder esclarecer suas dúvidas ou nos deixar comentários, críticas e opiniões. Logo entraremos em contato. Ficamos muito gratos pelo seu contato.</span>
                            </p>
                            <!--                            <p style="margin:1.43em 0em 0em 0em;line-height:1.54929577464789;"><span-->
                            <!--                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);">Rua Amazonas 504</span>-->
                            <!--                            </p>-->
                            <p style="margin:0em 0em 0em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);">Jaguariúna, São Paulo</span>
                            </p>
                            <p style="margin:1.43em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);">Phone No : +55 11 987115057</span>
                            </p>
                            <!--                            <p style="margin:0em 0em 0.36em 0em;line-height:1.54929577464789;"><span-->
                            <!--                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);">Fax no : +55 19 38378500</span>-->
                            <!--                            </p>-->
                            <p style="margin:0em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);">Email : contato@nsystem.com.br</span>
                            </p>
                            <p style="margin:0em 0em 0.36em 0em;line-height:1.54929577464789;"><br
                                        style="font-family:'Roboto','Arial';font-weight:500;font-size:1.143em;color:rgba(34,34,34,1);"/>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
        </div>
    </div>
    <div style="clear:both">
    </div>
</div>
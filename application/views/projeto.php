<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<nav id="ttr_menu" class="navbar-default navbar">
    <div id="ttr_menu_inner_in">
        <div class="menuforeground">
        </div>
        <div id="navigationmenu">
            <div class="navbar-header">
                <button id="nav-expander" data-target=".navbar-collapse" data-toggle="collapse"
                        class="navbar-toggle" type="button">
<span class="sr-only">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                    <span class="icon-bar">
</span>
                </button>
                <a href="http://www.nsystem.com.br/tcc/" target="_self">
                    <img class="ttr_menu_logo" src="<?php echo base_url("assets/img/menulogo.png") ?>">
                </a>
            </div>
            <div class="menu-center collapse navbar-collapse">
                <ul class="ttr_menu_items nav navbar-nav navbar-right">
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            Home</a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/empresa') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            EMPRESA
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/servico') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            SERVIÇO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown active"><a
                                href="<?php echo base_url('index.php/projeto') ?>"
                                class="ttr_menu_items_parent_link_active">
                            PROJETO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                    <li class="ttr_menu_items_parent dropdown"><a href="<?php echo base_url('index.php/contato') ?>"
                                                                  class="ttr_menu_items_parent_link">
                            CONTATO
                        </a>
                        <hr class="horiz_separator"/>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</nav>

<div style="margin:10px;padding:10px;" id="ttr_content_and_sidebar_container">
    <div id="ttr_content">
        <div id="ttr_content_margin" class="container-fluid">
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>

            <div class="ttr_Projects_html_row1 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column10">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="padding:40px;text-align:Center;line-height:3.16901408450704;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:3.429em;color:rgb(0,58,0);">AUTENTICAÇÃO DE PRESENÇA PARA AMBIENTES ESCOLARES</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>

            <!--            bloco-->
            <div class="ttr_Projects_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column20">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;">
                                <!--                                <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RECENT PROJECTS</span>-->
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column21">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Integrantes</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">NETO, Severiano Cezar</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Universidade de Jaguariúna</span>
                            </p>
                            <br>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">SOARES, Tonys Cruz Barbosa</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Universidade de Jaguariúna</span>
                            </p>
                            <br>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">SOUZA, Roberto Nogueira de</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Universidade de Jaguariúna</span>
                            </p>
                            <br>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">TORRES, André Coelho</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Universidade de Jaguariúna</span>
                            </p>
                            <br>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column22">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">.</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column23">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Resumo</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Este artigo irá demonstrar o desenvolvimento e implementação de um software de autenticação de presença para escolas e centros universitários, este software será responsável por gerar um código para o registro da presença e um aplicativo para dispositivos móveis, que será utilizado pelo corpo discente efetivar esse registro, fazendo com que o processo de chamada possa ser feito pelos alunos de maneira rápida e segura.
Demonstraremos a tecnologia utilizada para alcançar os objetivos propostos de forma confiável e com um baixo custo.
O softwares desenvolvidos tem o objetivo de, juntos, gerenciar e controlar a presença de estudantes em sala de aula em horário determinado bem como garantir uma maior agilidade no processo de chamada, gerando relatórios a partir das informações coletadas por meio destes registros.
Serão elencadas as ferramentas de software utilizadas para a elaboração dos dois softwares deste projeto que poderá ser disponibilizado para o uso em qualquer ambiente educacional conectado a internet que possua um terminal para a leitura do código.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column24">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Abstract</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">This article will demonstrate the development and implementation of presence authentication software for schools and university centers, this software will be responsible for generating a presence registration code and a mobile application that will be used by the student body to effect this registration. , so that the calling process can be done by students quickly and safely.
We will demonstrate the technology used to achieve the proposed objectives reliably and at low cost.
The software developed aims to jointly manage and control the presence of students in the classroom at a given time, as well as ensuring greater agility in the calling process, generating reports from the information collected through these records.
The software tools used for the elaboration of the two softwares of this project will be listed that can be made available for use in any educational environment connected to the internet that has a terminal for reading the code.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

            </div>

            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <!--            fim-bloco-->

            <!--            bloco-->
            <div class="ttr_Projects_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column20">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;">
                                <!--                                <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RECENT PROJECTS</span>-->
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column21">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Palavras-chaves</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Autenticação; presença; escolar.</span>
                            </p>
                            <br>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>

                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column22">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Key-words</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Authentication; presence; scholar.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>

                <!--                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">-->
                <!--                </div>-->
                <!---->
                <!--                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">-->
                <!--                    <div class="ttr_Projects_html_column23">-->
                <!--                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
                <!--                        <div class="html_content"><p-->
                <!--                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span-->
                <!--                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">.</span>-->
                <!--                            </p>-->
                <!--                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span-->
                <!--                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">.</span>-->
                <!--                            </p></div>-->
                <!--                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
                <!--                        <div style="clear:both;"></div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!---->
                <!--                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">-->
                <!--                </div>-->
                <!---->
                <!--                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">-->
                <!--                    <div class="ttr_Projects_html_column24">-->
                <!--                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
                <!--                        <div class="html_content"><p-->
                <!--                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span-->
                <!--                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">.</span>-->
                <!--                            </p>-->
                <!--                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span-->
                <!--                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">.</span>-->
                <!--                            </p></div>-->
                <!--                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>-->
                <!--                        <div style="clear:both;"></div>-->
                <!--                    </div>-->
                <!--                </div>-->
                <!---->
                <!--                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">-->
                <!--                </div>-->
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <!--            fim-bloco-->

            <!--            bloco-->
            <div class="ttr_Projects_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column20">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;">
                                <!--                                <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RECENT PROJECTS</span>-->
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column21">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Introdução</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">O conhecimento tem a ver com a evolução da técnica e das ciências (VARGAS LLOSA, 2012), essa evolução faz com que possamos buscar novos meios para executar algumas tarefas, meios com os quais podemos tornar essas tarefas mais eficientes e precisas.
O processo de determinação de presença em ambientes escolares é dos mais antigos, como um professor, através de papel ou meio eletrônico, informando se o aluno está ou não presente.  Este processo, no mínimo, demanda tempo, como também é um processo com falhas: o professor pode esquecer, o aluno pode responder e sair, atestando-se algo que não é verdade.
Tendo isto em vista será trabalhado um método pelo qual se pode registrar a presença de alunos em sala de aula, com a determinação de horários específicos, e a partir dos dados registrados obter relatórios destes dados.
Esse artigo visa explorar o tema da autenticação via a utilização de QRcode em dispositivo móvel, que segundo a DENSO WARE, empresa idealizadora desta tecnologia, surgiu como uma resposta às necessidades dos tempos (DENSO WAVE, 2019).
O texto está organizado da seguinte forma: na Seção 1, são apresentados os problemas associados aos métodos de realização do registro de presença de alunos em sala de aula; na Seção 2 é apresentado o QRcode e suas peculiaridades ; e na Seção 3 será apresentado o desenvolvimento do software, as ferramentas utilizadas e o seu resultado.</span>
                            </p>
                            <br>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column22">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">1.	Métodos de autenticação eletrônica</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Uma vez que os métodos tradicionais de autenticação possuem elementos facilmente perdidos, compartilhados, manipulados ou roubados, comprometendo a segurança (VIOLATO, 2013), será utilizado para este estudo um método de autenticação que é baseado no auxílio de meios eletrônicos uma vez que estes métodos não apresentam os inconvenientes citados em sua totalidade e tornam o ato da autenticação mais confiável.
Temos neste escopo a utilização da autenticação através de senha, com o auxílio de dispositivos externos, que se comunicam com outros meios eletrônicos, e através de sistemas biométricos, que podem usar uma gama de características físicas ou comportamentais de um indivíduo (J. L. Wayman, 2012).</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column23">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">1.1	Características dos métodos</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">São elencados aqui métodos de autenticação eletrônica.
Autenticação por meio de senha: Este é um método amplamente utilizado em todos os segmentos que utilizam armazenamento eletrônico, comércio eletrônico, controle de acesso, etc..
Ele pode ser considerado de baixa confiabilidade por ter a possibilidade do usuário esquecer a senha ou mesmo compartilhá-la com alguém que não tenha a efetiva autorização para utilização do serviço ou recurso a ela associada.
Cartões: O uso dos cartões é muito popular em diversas instituições mas também é sucetível a perdas, esquecimento e a ser cedido de forma indevida a pessoas sem a devida autorização.
Biometria: Ela se caracteriza por apresentar a possibilidade de medir os traços das pessoas e assim computá-los, gerando um identificador biométrico único, difícil de compartilhar, roubar, forjar e de ser alterado (COSTA, 2006), tornando-o assim um método de grande confiabilidade e segurança.
Beacons: O Beacon, ou farol, é um dispositivo que emite informações via rádio em intervalos definidos para smartphones, tablets, ou qualquer dispositivo compatível por meio da tecnologia Bluetooth Low Energy (Bluetooth de Baixa Energia) (RECK, 2017), é um método que também esbarra nas questões de perda, esquecimento e a indevida utilização.
QRcode: O Quick Response Code é um código de barras unidimensional que  possibilita inserção de dados que podem ser lidos e interpretados por smartphones (DE SOUZA CORRÊA, 2012), este método restringe o uso do aparelho ao seu proprietário, sendo o empréstimo destes aparelhos pouco comum, e também minimiza a questão do esquecimento do hardware de interação com o método de autenticação.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column24">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">2.	Meio viável</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Alguns dos métodos elencados podem ser utilizados para a implementação de um sistema eletrônico de controle de presença confiável e seguro.
Para o nosso estudo foi adotada a utilização do código de barras unidimensional de resposta rápida, o QRcode, uma vez que serão utilizados hardwares, softwares e recursos de armazenamento ou gratuitos ou já pertencentes aos membros do estudo.
Estes recursos serão os smartphones dos participantes, seus notebooks e uma hospedagem para o website.
Será desenvolvido um software em java, através da IDE AndroidStudio, para efetuar a verificação da presença do aluno e um website, desenvolvido com o auxílio da IDE Visual Studio Code, para a geração do código verificador, relatórios e armazenamento de dados em um banco de dados MySql.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <!--            fim-bloco-->

            <!--            bloco-->
            <div class="ttr_Projects_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column20">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;">
                                <!--                                <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RECENT PROJECTS</span>-->
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column21">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">2.1	Características do QRcode</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Segundo os criadores desta tecnologia (DENSO WAVE, 2019) elencamos a seguir algumas de suas características.
•	Dados de codificação de alta capacidade:
Códigos de barras convencionais armazenam, no máximo, aproximadamente 20 dígitos, o QR Code é capaz de lidar com várias dezenas a centenas de vezes mais informações.
Ele é capaz de manipular todos os tipos de dados, como caracteres numéricos e alfabéticos, Kanji, Kana, Hiragana, símbolos, códigos binários e de controle. Até 7.089 caracteres podem ser codificados em um símbolo.
•	Pequeno tamanho de impressão:
Ele carrega informações tanto horizontal como verticalmente, o QR Code é capaz de codificar a mesma quantidade de dados em aproximadamente um décimo do espaço de um código de barras tradicional. (Para um tamanho de impressão menor, o Micro QR Code está disponível.)
•	Capacidade de entendimento de Kanji e Kana:
Como uma simbologia desenvolvida no Japão, o QR Code é capaz de codificar o conjunto de caracteres kanji dos níveis 1 e 2 do JIS(Japanese Industrial Standards). No caso do japonês, um caractere Kana ou Kanji de largura total é codificado eficientemente em 13 bits, permitindo que o QR Code armazene mais de 20% dos dados que outras simbologias 2D.
•	Correção de erros:
Tem capacidade de correção de erros. Os dados podem ser restaurados mesmo se o símbolo estiver parcialmente sujo ou danificado. Um máximo de 30% de palavras de código podem ser restauradas.
•	Ângulo de leitura de 360°:
A capacidade de leitura de alta velocidade é de 360 graus (omnidirecional). O QR Code realiza essa tarefa por meio de padrões de detecção de posição localizados nos três cantos do símbolo. Esses padrões de detecção de posição garantem uma leitura estável de alta velocidade, contornando os efeitos negativos da interferência de fundo.
•	Recurso de anexação estruturada:
Ele pode ser dividido em várias áreas de dados. Por outro lado, as informações armazenadas em vários símbolos de QR Code podem ser reconstruídas como um único símbolo de dados. Um símbolo de dados pode ser dividido em até 16 símbolos, permitindo a impressão em uma área estreita.</span>
                            </p>
                            <br>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column22">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">3.	Visão geral</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">O estudo surgiu com o objetivo de fazer com que o processo da autenticação da chamada em sala de aula fosse feito de forma automática, segura e a partir dele pudessem ser emitidos relatórios destas chamadas.
A solução desenvolvida é composta pelos seguintes componentes:
Website desenvolvido com as tecnologias Html5, CSS, PHP, JavaScript – que controla a interação com o smartphone do usuário através de um gerador de  QRcode que contém os dados da autenticação a ser efetivada e gerencia o banco de dados MySql integrado;
Aplicativo para telefone Android – responsável pela autenticação do usuário através da utilização da câmera do aparelho para a leitura do QRcode gerado e envio dos dados para o servidor;</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column23">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">4.	Implementação</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">A implementação serra concretizada em três etapas:
 1ª etapa:
Criação de um aplicativo que será feito através da linguagem java, com o auxílio da IDE Visual Studio code, em sua estrutura o aplicativo terá métodos, classes, estruturas de decisão e repetição além de atributos específicos para a verificação de QRcodes e que garantem a segurança da informação verificada .
2ª etapa:
Elaboração e criação de um website em HTML5,JAVA e CSS, conterá páginas que só serão acessadas com o nome do aluno e RA ou com senhas especiais que serão de uso apenas dos docentes, em sua estrutura terão campos para busca e cadastramento de novos estudantes e ferramentas para gerar relatórios de frequência e QRcodes contendo dados e informações dos usuários.
Vai ser gerado um código QRcode com o uso do PHP, a partir de informações coletadas de websites que demonstram a utilização das tecnologias citadas e livrarias específicas para o QRcode (Brocanelli, 2019) (Thiengo, 2019), este código é gerado contendo as informações de data e horário e as informações da aula a partir dos dados armazenados, este código ficará disponível na tela do docente para que possa ser lido através do aplicativo Android criado pelo grupo de estudo.
Ao ler esse código o aplicativo retorna ao sistema, através da internet, um código que registra sua entrada ou saída da aula.
3ª etapa:
Construir um banco de dados em Mysql, sua elaboração conterá classe de alunos, classes de  professores e chaves que garantem sua integridade, sua principal função será receber e gerenciar informações providas do website e armazenar em sua estrutura.
Serão usadas as tecnologias descritas a seguir:
“Html5 é a mais recente evolução do padrão que define o HTML. O termo representa dois conceitos diferentes:
É uma nova versão da linguagem HTML, com novos elementos, atributos, e comportamentos e um conjunto maior de tecnologias que permite o desenvolvimento de aplicações e web sites mais diversos e poderosos. Este conjunto é chamado HTML5 & friends e muitas vezes abreviado apenas como HTML5.” (Mozilla e colaboradores individuais, 2019)
“CSS3 é a terceira mais nova versão das famosas Cascading Style Sheets (ou simplesmente CSS), onde se define estilos para seu projeto web.Com efeitos de transição, imagem,imagem de fundo/Background e outros, que dão um estilo novo e elegante a seus projetos web.Ou em todos os aspectos de design do layout da página.” (Wikipédia, a enciclopédia livre., 2019)
“PHP é uma linguagem de script open source de uso geral, muito utilizada, e especialmente adequada para o desenvolvimento web e que pode ser embutida dentro do HTML.” (O que é o PHP?, 2019)
“JavaScript frequentemente abreviado como JS, é uma linguagem de programação interpretada de alto nível, caracterizada também,como dinâmica, fracamente tipada, ...JavaScript permite páginas da Web interativas e, portanto, é uma parte essencial dos aplicativos da web.” (Wikipédia, a enciclopédia livre., 2019)
“Java é uma linguagem de programação e plataforma computacional lançada pela primeira vez pela Sun Microsystems em 1995. Existem muitas aplicações e sites que não funcionarão, a menos que você tenha o Java instalado, e mais desses são criados todos os dias. O Java é rápido, seguro e confiável. De laptops a datacenters, consoles de games a supercomputadores científicos, telefones celulares à Internet, o Java está em todos os lugares!” (Oracle Corporation, 2019)</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column24">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Resultados Esperados</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Este artigo demonstra uma solução para a efetivação da autenticação da presença de alunos em sala de aula, gerando uma maior segurança e praticidade no que se referir a controle de chamadas, de maneira a permitir que estas autenticações sejam registradas em banco de dados assegurando um gerenciamento bem mais eficaz não só pelo próprio aluno mas também pelos docentes, e a partir das informações armazenadas no banco emitir os relatórios necessários para análise da instituição e controle pessoal do aluno.
Com a utilização de um QRcode gerado no website é possível transmitir nele os dados da aula em curso e ao ler este código, por meio do smartphone, o aluno confirma a sua presença a partir do retorno de um identificador único incluído no código de retorno, em seguida o sistema grava as informações no banco de dados,garantindo uma simplificação no processo e um ganho significativo de tempo.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <!--            fim-bloco-->

            <!--            bloco-->
            <div class="ttr_Projects_html_row2 row">
                <div class="post_column col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column20">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="text-align:Center;">
                                <!--                                <span style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:2.571em;color:rgba(1,156,204,1);">RECENT PROJECTS</span>-->
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column21">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p style="line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">Referências Bibliográficas</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">Brocanelli, D. - Gerando QR Codes com PHP + MySQL + PHP QR Code - Diego Brocanelli.  Disponível em:
< http://www.diegobrocanelli.com.br/php/gerando-qr-codes-com-php-mysql-php-qr-code/ >. Acesso em: 20 mai. 2019.

COSTA, Luciano R.; OBELHEIRO, Rafael R.; FRAGA, Joni S. Introdução a biometria. Livro texto dos Minicursos do VI Simpósio Brasileiro de Segurança da Informação e de Sistemas Computacionais (SBSeg2006). SBC: Porto Alegre, v. 1, p. 103-151, 2006.

DE SOUZA CORRÊA, Maria Iraê; DE SOUZA, Angela Cristina Rocha; MARÇAL, Maria Christianni Coutinho. O uso do QR CODE na gestão da comunicação: o caso da rede social WineTag. Informe: Estudos em Biblioteconomia e Gestão da Informação, v. 1, n. 1, p. 118-132.. 2012

DENSO WAVE. Answer to your question about the QR Code. Disponível em:
< http://www.qrcode.com/en/ >. Acesso em: 07 mai. 2019.

HTML5 - HTML | MDN. Disponível em:
< https://developer.mozilla.org/pt-BR/docs/Web/HTML/HTML5 >. Acesso em: 20 mai. 2019.

J. L. Wayman, A. K. Jain, D. Maltoni e D. Maio (editors). Biometric Systems:
Technology, Design and Performance Evaluation. Springer, 2005.

O que é o PHP? - PHP. Disponível em:
< https://www.php.net/manual/pt_BR/intro-whatis.php >. Acesso em: 20 mai. 2019.

Oracle Corporation - O que é o Java e porque preciso dele?. Disponível em:
< https://www.java.com/pt_BR/download/faq/whatis_java.xml >. Acesso em: 20 mai. 2019.

RECK, Marcelo Sala. Beacons BLE-Bluetooth Low Energy-design e análise de um sistema de localização indoor. 2017.

Thiengo, V. - Criando QR Code no PHP. Disponível em:
< https://www.thiengo.com.br/criando-qr-code-no-php >. Acesso em: 20 mai. 2019.

VARGAS LLOSA, Mario. A civilização de espetáculo: uma radiografia do nosso tempo e da nossa cultura. Rio de Janeiro: editora Objetiva, 2012.

VIOLATO, Ricardo Paranhos Velloso et al. MULTIBIOMETRIA: UMA VISÃO GERAL E APLICADA A FACE E VOZ. 2013.

Wikipédia, a enciclopédia livre - CSS3 - Wikipédia. Disponível em:
< https://pt.wikipedia.org/wiki/CSS3 >. Acesso em: 20 mai. 2019.

Wikipédia, a enciclopédia livre - JavaScript - Wikipédia. Disponível em:
< https://pt.wikipedia.org/wiki/JavaScript >. Acesso em: 20 mai. 2019.
</span>
                            </p>
                            <br>
                        </div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column22">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;">.</span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;">.</span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column23">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;"> </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;"> </span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-sm-block visible-md-block visible-xs-block">
                </div>
                <div class="post_column col-lg-6 col-md-12 col-sm-12 col-xs-12">
                    <div class="ttr_Projects_html_column24">
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div class="html_content"><p
                                    style="margin:0.36em 0em 0.36em 0em;line-height:1.97183098591549;"><span
                                        style="font-family:'Roboto Slab','Arial';font-weight:700;font-size:1.429em;"> </span>
                            </p>
                            <p style="margin:0.71em 0em 0.36em 0em;line-height:1.54929577464789;"><span
                                        style="font-family:'Roboto','Arial';font-weight:300;font-size:1.143em;"> </span>
                            </p></div>
                        <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
                        <div style="clear:both;"></div>
                    </div>
                </div>
                <div class="clearfix visible-lg-block visible-sm-block visible-md-block visible-xs-block">
                </div>
            </div>
            <div style="height:0px;width:0px;overflow:hidden;-webkit-margin-top-collapse: separate;"></div>
            <!--            fim-bloco-->

        </div>
    </div>
    <div style="clear:both">
    </div>
</div>
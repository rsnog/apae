<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class AlunoModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos()
    {
        $this->db->select("*");
        $this->db->from("aluno");
//        $this->db->join("permissoes as p", "p.id = u.id_niveis", 'LEFT');
//        $this->db->order_by('usuario_id', 'ASC');
//        $this->db->order_by('usuario_nome', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function verifica_aulas($id, $nome)
    {
        $this->db->where('aluno_id', $id);
        $this->db->like('aluno_aula', $nome, 'both');
        $query = $this->db->get('aluno', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->aluno_aula;
        else:
            return null;
        endif;
    }

//    public function listar_todos_documentos($id)
//    {
//        $this->db->select("*");
//        $this->db->from("arquivo_usuario as a");
//        $this->db->join("usuario as u", "u.id = a.id_usuario", 'LEFT');
//        $this->db->where('id_usuario', $id);
//        $resultado = $this->db->get()->result();
//        return $resultado;
//    }

//    public function get_email($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_email;
//        else:
//            return null;
//        endif;
//    }

//    public function get_senha($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_senha;
//        else:
//            return null;
//        endif;
//    }

//    public function get_nome($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_nome;
//        else:
//            return null;
//        endif;
//    }

//    public function get_habilitado($id)
//    {
//        $this->db->where('aluno_id', $id);
//        $query = $this->db->get('aluno', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->aluno_habilitado;
//        else:
//            return null;
//        endif;
//    }

//    public function get_perfil($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->id_niveis;
//        else:
//            return null;
//        endif;
//    }

    public function update_habilitado($id, $habilitado)
    {
        $this->db->where('aluno_id', $id);
        $query = $this->db->get('aluno', 1);
        if ($query->num_rows() == 1):
            $this->db->where('aluno_id', $id);
            $this->db->set('aluno_habilitado', $habilitado);
            $this->db->update('aluno');
            return $this->db->affected_rows();
        else:
            $error = $this->db->error();
            return $error;
        endif;
    }

//    public function get_nivel($id)
//    {
//        $this->db->where('id', $id);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->id_niveis;
//        else:
//            return null;
//        endif;
//    }

//    public function get_id($nome, $email)
//    {
//        $this->db->where('usuario_nome', $nome);
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_id;
//        else:
//            return null;
//        endif;
//    }

    public function update_aluno($id, $nome, $ra, $telefone, $aula)
    {
        $this->db->db_debug = true;
        $this->db->where('aluno_id', $id);
        $query = $this->db->get('aluno', 1);
        if ($query->num_rows() == 1):
            $this->db->where('aluno_id', $id);
            $this->db->set('aluno_nome', $nome);
            $this->db->set('aluno_ra', $ra);
            $this->db->set('aluno_telefone', $telefone);
            $this->db->set('aluno_aula', $aula);
            $this->db->update('aluno');
            return $this->db->affected_rows();
        else:
            $error = $this->db->error();
            return $error;
        endif;
    }

//    public function update_user_senha($id, $nome, $email, $senha, $id_niveis)
//    {
//        $this->db->where('usuario_id', $id);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $this->db->where('usuario_id', $id);
//            $this->db->set('usuario_nome', $nome);
//            $this->db->set('usuario_email', $email);
//            $this->db->set('usuario_senha', $senha);
//            $this->db->set('id_niveis', $id_niveis);
//            $this->db->update('usuario');
//            return $this->db->affected_rows();
//        else:
//            return NULL;
//        endif;
//    }

//    public function foto_padrao()
//    {
//        $this->db->where('id_usuario', 1);
//        $this->db->where('arquivo_tamanho', 216762);
//        $this->db->where('arquivo_tipo', 'image/png');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function foto_usuario($id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function foto_usuario_tipo($id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_tipo;
//        else:
//            return null;
//        endif;
//    }
//
//    public function arquivo_usuario($nome, $tamanho, $id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function arquivo_usuario_tipo($nome, $tamanho, $id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_tipo;
//        else:
//            return null;
//        endif;
//    }
//
//    public function update_user_arquivo($nome, $dados, $tipo, $tamanho, $dataHora, $idUser)
//    {
//        $this->db->where('id', $idUser);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $this->db->where('id', $idUser);
//            $this->db->where('arquivo_descricao', 'Foto');
//            $this->db->set('arquivo_nome', $nome);
//            $this->db->set('arquivo_dados', $dados);
//            $this->db->set('arquivo_tipo', $tipo);
//            $this->db->set('arquivo_tamanho', $tamanho);
//            $this->db->set('arquivo_dtHrEnvio', $dataHora);
//            $this->db->update('arquivo_usuario');
//            return $this->db->affected_rows();
//        else:
//            return NULL;
//        endif;
//    }
//
//    public function create_user_arquivo($nome, $descricao, $dados, $tipo, $tamanho, $dataHora, $idUser)
//    {
//        $dados = array(
//            'arquivo_nome' => $nome,
//            'arquivo_descricao' => $descricao,
//            'arquivo_dados' => $dados,
//            'arquivo_tipo' => $tipo,
//            'arquivo_tamanho' => $tamanho,
//            'arquivo_dtHrEnvio' => $dataHora,
//            'id_usuario' => $idUser,
//        );
//        $this->db->insert('arquivo_usuario', $dados);
//        return $this->db->insert_id();
//    }

    public function create_aluno($ra, $nome, $telefone)
    {
        $dados = array(
            'aluno_ra' => $ra,
            'aluno_nome' => $nome,
            'aluno_telefone' => $telefone,
            'aluno_habilitado' => 1,
        );
        $this->db->insert('aluno', $dados);
        return $this->db->insert_id();
    }

//    public function deletar_arquivo($id_usuario, $nome, $tamanho)
//    {
//        $this->db->where('id_usuario', $id_usuario);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $this->db->delete('arquivo_usuario');
//        return $this->db->affected_rows();
//    }
//
//    public function limpar_tabela()
//    {
//        $this->db->empty_table('arquivo_usuario');
//        $this->db->query("ALTER TABLE arquivo_usuario AUTO_INCREMENT = 1;");
//        return $this->db->affected_rows();
//    }
}
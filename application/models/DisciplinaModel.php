<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class DisciplinaModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos_disciplina()
    {
        $this->db->select("*");
        $this->db->from("disciplina as d");
        $this->db->join("usuario as u", "u.usuario_id = d.id_docente", 'LEFT');
        $this->db->order_by('id_docente', 'DESC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_disciplina_usuario($usuario_id)
    {
        $this->db->select("*");
        $this->db->from("disciplina as d");
        $this->db->join("usuario as u", "u.usuario_id = d.id_docente", 'LEFT');
        $this->db->where('usuario_id', $usuario_id);
        $this->db->order_by('id_docente', 'DESC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function get_habilitado($id)
    {
        $this->db->where('disciplina_id', $id);
        $query = $this->db->get('disciplina', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->disciplina_habilitado;
        else:
            return NULL;
        endif;
    }

    public function get_nome($id)
    {
        $this->db->select("*");
        $this->db->from("disciplina");
        $this->db->where('disciplina_id', $id);
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function update_habilitado($id, $habilitado)
    {
        $this->db->where('disciplina_id', $id);
        $query = $this->db->get('disciplina', 1);
        if ($query->num_rows() == 1):
            $this->db->where('disciplina_id', $id);
            $this->db->set('disciplina_habilitado', $habilitado);
            $this->db->update('disciplina');
            return $this->db->affected_rows();
        else:
            return NULL;
        endif;
    }

    public function update_disciplina($id, $nome, $docente)
    {
//        $this->db->db_debug = false;
        $this->db->where('disciplina_id', $id);
        $query = $this->db->get('disciplina', 1);
        if ($query->num_rows() == 1):
            $this->db->where('disciplina_id', $id);
            $this->db->set('disciplina_nome', $nome);
            $this->db->set('id_docente', $docente);
            $this->db->update('disciplina');
            return $this->db->affected_rows();
        else:
//            $error = $this->db->error();
//            return $error;
            return NULL;
        endif;
    }

    public function create_disciplina($nome, $docente)
    {
        $dados = array(
            'disciplina_nome' => $nome,
            'disciplina_habilitado' => 1,
            'id_docente' => $docente,
        );
        $this->db->insert('disciplina', $dados);
        return $this->db->insert_id();
    }

}
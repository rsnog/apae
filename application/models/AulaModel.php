<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class AulaModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos()
    {
        $this->db->select("*");
        $this->db->from("aula as a");
        $this->db->join("diadasemana as d", "d.dia_id = a.dia_aula", 'LEFT');
        $this->db->join("disciplina as s", "s.disciplina_id = a.disciplina_aula", 'LEFT');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_dias()
    {
        $this->db->select("*");
        $this->db->from("diadasemana");
        $this->db->order_by('dia_id', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

//    public function listar_todos_documentos($id)
//    {
//        $this->db->select("*");
//        $this->db->from("arquivo_usuario as a");
//        $this->db->join("usuario as u", "u.id = a.id_usuario", 'LEFT');
//        $this->db->where('id_usuario', $id);
//        $resultado = $this->db->get()->result();
//        return $resultado;
//    }

//    public function get_email($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_email;
//        else:
//            return null;
//        endif;
//    }

//    public function get_senha($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_senha;
//        else:
//            return null;
//        endif;
//    }

//    public function get_nome($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_nome;
//        else:
//            return null;
//        endif;
//    }

//    public function get_habilitado($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_habilitado;
//        else:
//            return null;
//        endif;
//    }

//    public function get_perfil($email)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->id_niveis;
//        else:
//            return null;
//        endif;
//    }

//    public function update_habilitado($email, $habilitado)
//    {
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $this->db->where('usuario_email', $email);
//            $this->db->set('usuario_habilitado', $habilitado);
//            $this->db->update('usuario');
//            return $this->db->affected_rows();
//        else:
//            return null;
//        endif;
//    }

//    public function get_nivel($id)
//    {
//        $this->db->where('id', $id);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->id_niveis;
//        else:
//            return null;
//        endif;
//    }

//    public function get_id($nome, $email)
//    {
//        $this->db->where('usuario_nome', $nome);
//        $this->db->where('usuario_email', $email);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->usuario_id;
//        else:
//            return null;
//        endif;
//    }

    public function update_aula($id, $inicio, $fim, $dia, $disciplina)
    {
        $this->db->where('aula_id', $id);
        $query = $this->db->get('aula', 1);
        if ($query->num_rows() == 1):
            $this->db->where('aula_id', $id);
            $this->db->set('hora_inicio', $inicio);
            $this->db->set('hora_fim', $fim);
            $this->db->set('dia_aula', $dia);
            $this->db->set('disciplina_aula', $disciplina);
            $this->db->update('aula');
            return $this->db->affected_rows();
        else:
            return NULL;
        endif;
    }

//    public function update_user_senha($id, $nome, $email, $senha, $id_niveis)
//    {
//        $this->db->where('usuario_id', $id);
//        $query = $this->db->get('usuario', 1);
//        if ($query->num_rows() == 1):
//            $this->db->where('usuario_id', $id);
//            $this->db->set('usuario_nome', $nome);
//            $this->db->set('usuario_email', $email);
//            $this->db->set('usuario_senha', $senha);
//            $this->db->set('id_niveis', $id_niveis);
//            $this->db->update('usuario');
//            return $this->db->affected_rows();
//        else:
//            return NULL;
//        endif;
//    }

//    public function foto_padrao()
//    {
//        $this->db->where('id_usuario', 1);
//        $this->db->where('arquivo_tamanho', 216762);
//        $this->db->where('arquivo_tipo', 'image/png');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function foto_usuario($id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function foto_usuario_tipo($id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_tipo;
//        else:
//            return null;
//        endif;
//    }
//
//    public function arquivo_usuario($nome, $tamanho, $id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_dados;
//        else:
//            return null;
//        endif;
//    }
//
//    public function arquivo_usuario_tipo($nome, $tamanho, $id)
//    {
//        $this->db->where('id_usuario', $id);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $row = $query->row();
//            return $row->arquivo_tipo;
//        else:
//            return null;
//        endif;
//    }
//
//    public function update_user_arquivo($nome, $dados, $tipo, $tamanho, $dataHora, $idUser)
//    {
//        $this->db->where('id', $idUser);
//        $this->db->where('arquivo_descricao', 'Foto');
//        $query = $this->db->get('arquivo_usuario', 1);
//        if ($query->num_rows() == 1):
//            $this->db->where('id', $idUser);
//            $this->db->where('arquivo_descricao', 'Foto');
//            $this->db->set('arquivo_nome', $nome);
//            $this->db->set('arquivo_dados', $dados);
//            $this->db->set('arquivo_tipo', $tipo);
//            $this->db->set('arquivo_tamanho', $tamanho);
//            $this->db->set('arquivo_dtHrEnvio', $dataHora);
//            $this->db->update('arquivo_usuario');
//            return $this->db->affected_rows();
//        else:
//            return NULL;
//        endif;
//    }
//
//    public function create_user_arquivo($nome, $descricao, $dados, $tipo, $tamanho, $dataHora, $idUser)
//    {
//        $dados = array(
//            'arquivo_nome' => $nome,
//            'arquivo_descricao' => $descricao,
//            'arquivo_dados' => $dados,
//            'arquivo_tipo' => $tipo,
//            'arquivo_tamanho' => $tamanho,
//            'arquivo_dtHrEnvio' => $dataHora,
//            'id_usuario' => $idUser,
//        );
//        $this->db->insert('arquivo_usuario', $dados);
//        return $this->db->insert_id();
//    }

    public function create_aula($inicio, $fim, $dia, $disciplina)
    {
        $dados = array(
            'hora_inicio' => $inicio,
            'hora_fim' => $fim,
            'dia_aula' => $dia,
            'disciplina_aula' => $disciplina,
        );
        $this->db->insert('aula', $dados);
        return $this->db->insert_id();
    }

//    public function deletar_arquivo($id_usuario, $nome, $tamanho)
//    {
//        $this->db->where('id_usuario', $id_usuario);
//        $this->db->where('arquivo_nome', $nome);
//        $this->db->where('arquivo_tamanho', $tamanho);
//        $this->db->delete('arquivo_usuario');
//        return $this->db->affected_rows();
//    }
//
//    public function limpar_tabela()
//    {
//        $this->db->empty_table('arquivo_usuario');
//        $this->db->query("ALTER TABLE arquivo_usuario AUTO_INCREMENT = 1;");
//        return $this->db->affected_rows();
//    }
}
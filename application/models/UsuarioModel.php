<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class UsuarioModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos()
    {
        $this->db->select("*");
        $this->db->from("usuario as u");
        $this->db->join("permissoes as p", "p.id = u.id_niveis", 'LEFT');
        $this->db->order_by('usuario_id', 'ASC');
        $this->db->order_by('usuario_nome', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function get_email($email)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->usuario_email;
        else:
            return null;
        endif;
    }

    public function get_senha($email)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->usuario_senha;
        else:
            return null;
        endif;
    }

    public function get_nome($email)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->usuario_nome;
        else:
            return null;
        endif;
    }

    public function get_habilitado($email)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->usuario_habilitado;
        else:
            return null;
        endif;
    }

    public function get_perfil($email)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->id_niveis;
        else:
            return null;
        endif;
    }

    public function update_habilitado($email, $habilitado)
    {
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $this->db->where('usuario_email', $email);
            $this->db->set('usuario_habilitado', $habilitado);
            $this->db->update('usuario');
            return $this->db->affected_rows();
        else:
            return null;
        endif;
    }

    public function get_id($nome, $email)
    {
        $this->db->where('usuario_nome', $nome);
        $this->db->where('usuario_email', $email);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->usuario_id;
        else:
            return null;
        endif;
    }

    public function update_user($id, $nome, $email, $id_niveis)
    {
        $this->db->where('usuario_id', $id);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $this->db->where('usuario_id', $id);
            $this->db->set('usuario_nome', $nome);
            $this->db->set('usuario_email', $email);
            $this->db->set('id_niveis', $id_niveis);
            $this->db->update('usuario');
            return $this->db->affected_rows();
        else:
            return NULL;
        endif;
    }

    public function update_user_senha($id, $nome, $email, $senha, $id_niveis)
    {
        $this->db->where('usuario_id', $id);
        $query = $this->db->get('usuario', 1);
        if ($query->num_rows() == 1):
            $this->db->where('usuario_id', $id);
            $this->db->set('usuario_nome', $nome);
            $this->db->set('usuario_email', $email);
            $this->db->set('usuario_senha', $senha);
            $this->db->set('id_niveis', $id_niveis);
            $this->db->update('usuario');
            return $this->db->affected_rows();
        else:
            return NULL;
        endif;
    }

    public function create_user($nome, $email, $senha, $perfil)
    {
        $dados = array(
            'usuario_nome' => $nome,
            'usuario_email' => $email,
            'usuario_senha' => $senha,
            'usuario_habilitado' => 1,
            'id_niveis' => $perfil,
        );
        $this->db->insert('usuario', $dados);
        return $this->db->insert_id();
    }

}
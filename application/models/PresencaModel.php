<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class PresencaModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos()
    {
        $this->db->select("*");
        $this->db->from("presenca");
        $resultado = $this->db->get()->result();
        return $resultado;
    }

//    public function listar_todos_docentes_disciplina()
//    {
//        $this->db->select("*");
//        $this->db->from("usuario as u");
//        $this->db->join("disciplina as d", "d.id_docente = u.usuario_id", 'LEFT');
//        $this->db->having('id_docente !=', NULL);
//        $resultado = $this->db->get()->result();
//        return $resultado;

//    }

    public function listar_todos_alunos_presenca()
    {
        $this->db->select("*");
        $this->db->from("aluno as al");
//        $this->db->join("aluno_presenca as ap", "al.id_presenca = ap.aluno_presenca_id", 'LEFT');
//        $this->db->join("disciplina as d", "ap.id_disciplina = d.disciplina_id", 'LEFT');
//        $this->db->join("aula as au", "ap.id_aula = au.aula_id", 'LEFT');
//        $this->db->join("diadasemana as ds", "ds.dia_id = au.dia_aula", 'LEFT');
//        $this->db->join("usuario as u", "d.id_docente = u.usuario_id", 'LEFT');
        $this->db->order_by('aluno_nome', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_alunos_presenca_disciplina($disciplina)
    {
        $this->db->select("*");
        $this->db->from("aluno as al");
        $this->db->join("presenca as ap", "al.id_presenca = ap.presenca_id", 'LEFT');
//        $this->db->join("aluno_presenca as ap", "al.id_presenca = ap.aluno_presenca_id", 'LEFT');
//        $this->db->join("disciplina as d", "ap.id_disciplina = d.disciplina_id", 'LEFT');
//        $this->db->join("aula as au", "ap.id_aula = au.aula_id", 'LEFT');
//        $this->db->join("diadasemana as ds", "ds.dia_id = au.dia_aula", 'LEFT');
//        $this->db->join("usuario as u", "d.id_docente = u.usuario_id", 'LEFT');
        $this->db->like('aluno_aula', $disciplina);
        $this->db->order_by('aluno_nome', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_alunos_presenca_disciplina_data($disciplina, $data)
    {
        $this->db->select("*");
        $this->db->from("presenca as p");
        $this->db->join("aluno as a", "p.id_aluno_presenca = a.aluno_id", 'LEFT');
        $this->db->where('disciplina', $disciplina);
        $this->db->where('presenca_data', $data);
        $this->db->order_by('aluno_nome', 'ASC');
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function verificar_presenca($data, $aluno_id, $disciplina)
    {
        $this->db->where('presenca_data', $data);
        $this->db->where('id_aluno_presenca', $aluno_id);
        $this->db->where('disciplina', $disciplina);
        $query = $this->db->get('presenca', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->presente;
        else:
            return 0;
        endif;
    }

    public function update_presenca($data, $id_aluno, $presente, $disciplina)
    {
        $this->db->where('presenca_data', $data);
        $this->db->where('id_aluno_presenca', $id_aluno);
        $this->db->where('disciplina', $disciplina);
        $query = $this->db->get('presenca', 1);
        if ($query->num_rows() == 1):
            $this->db->where('presenca_data', $data);
            $this->db->where('id_aluno_presenca', $id_aluno);
            $this->db->where('disciplina', $disciplina);
            $this->db->set('presente', $presente);
            $this->db->update('presenca');
            return $this->db->affected_rows();
        else:
            $dados = array(
                'presenca_data' => $data,
                'presente' => $presente,
                'id_aluno_presenca' => $id_aluno,
                'disciplina' => $disciplina,
            );
            $this->db->insert('presenca', $dados);
            return $this->db->insert_id();
        endif;
    }

    public function update_presenca2($data, $id_aluno, $presente, $disciplina)
    {
        $this->db->where('presenca_data', $data);
        $this->db->where('id_aluno_presenca', $id_aluno);
        $this->db->where('disciplina', $disciplina);
        $query = $this->db->get('presenca', 1);
        if ($query->num_rows() == 1):
            $this->db->where('presenca_data', $data);
            $this->db->where('id_aluno_presenca', $id_aluno);
            $this->db->where('disciplina', $disciplina);
            $this->db->set('presente', $presente);
            $this->db->update('presenca');
            return $this->db->affected_rows();
        else:
            $error = $this->db->error();
            return $error;
        endif;
    }

//    public function create_presenca($data, $id_aluno)
//    {
//        $dados = array(
//            'presenca_data' => $data,
//            'presente' => TRUE,
//            'id_aluno_presenca' => $id_aluno,
//        );
//        $this->db->insert('presenca', $dados);
//        return $this->db->insert_id();
//    }
//
//    public function update_check($data, $id_aluno, $presente)
//    {
//            $this->db->where('presenca_data', $data);
//            $this->db->where('id_aluno_presenca', $id_aluno);
//            $this->db->set('presente', $presente);
//            $this->db->update('presenca');
//            return $this->db->affected_rows();
//    }

    public function limpar_presenca()
    {
        $this->db->empty_table('presenca');
        $this->db->query("ALTER TABLE presenca AUTO_INCREMENT = 1;");
        return $this->db->affected_rows();
    }
}
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PermissaoModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos()
    {
        $this->db->select("*");
        $resultado = $this->db->get("permissoes")->result();
        return $resultado;
    }

    public function get_permissao_nome($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('permissoes', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->permissao_nome;
        else:
            return null;
        endif;
    }

    public function get_permissao_id($id)
    {
        $this->db->where('id', $id);
        $query = $this->db->get('permissoes', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->id;
        else:
            return null;
        endif;
    }

    public function get_permissao_id_nome($nome)
    {
        $this->db->where('permissao_nome', $nome);
        $query = $this->db->get('permissoes', 1);
        if ($query->num_rows() == 1):
            $row = $query->row();
            return $row->id;
        else:
            return null;
        endif;
    }
}
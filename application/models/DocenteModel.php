<?php

defined('BASEPATH') OR exit('No direct script access allowed');

//SET FOREIGN_KEY_CHECKS = 0;
//SET FOREIGN_KEY_CHECKS = 1;

class DocenteModel extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function listar_todos_docentes_disciplina()
    {
        $this->db->select("*");
        $this->db->from("usuario as u");
        $this->db->join("disciplina as d", "d.id_docente = u.usuario_id", 'LEFT');
        $this->db->having('id_docente !=', NULL);
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_docentes()
    {
        $this->db->select("*");
        $this->db->from("usuario");
        $this->db->where('id_niveis', 3);
        $resultado = $this->db->get()->result();
        return $resultado;
    }

    public function listar_todos_docentes_disciplina_aula()
    {
        $this->db->select("*");
        $this->db->from("usuario as u");
        $this->db->join("disciplina as d", "d.id_docente = u.usuario_id", 'LEFT');
        $this->db->join("aula as a", "a.disciplina_aula = d.disciplina_id", 'LEFT');
        $this->db->join("diadasemana as ds", "ds.dia_id = a.dia_aula", 'LEFT');
        $this->db->having('id_docente !=', NULL);
        $this->db->having('dia_aula !=', NULL);
        $resultado = $this->db->get()->result();
        return $resultado;
    }
}
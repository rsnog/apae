<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AulaController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model("AulaModel", "aula");
        $this->load->model("DisciplinaModel", "discip");
    }

    public function index()
    {
        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            $this->session->set_userdata('usuario_id', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->aula->listar_todos();
        $dados = array("titulo" => 'Aula', "nomeLogged" => $resultadoCon1, "aulaDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_aula', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function cadastrarAulaForm()
    {
        $infoAllDisciplina = $this->discip->listar_todos_disciplina();
        $optionsDisc = array('' => 'Selecione...');
        foreach ($infoAllDisciplina as $tDisc) {
            $optionsDisc[$tDisc->disciplina_id] = $tDisc->disciplina_nome;
        }
        $infoAllDia = $this->aula->listar_todos_dias();
        $optionsDia = array('' => 'Selecione...');
        foreach ($infoAllDia as $tDia) {
            $optionsDia[$tDia->dia_id] = $tDia->dia_nome;
        }
        echo form_open('cadAulaDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataHini = array(
            'name' => 'aula_inicio',
            'id' => 'aula_inicio_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Início'
        );
        $dataHfim = array(
            'name' => 'aula_fim',
            'id' => 'aula_fim_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Fim'
        );
        echo "<table style='padding: 0px; margin: 0px' class=\"table table-borderless\">
                        <thead>
                        <tr>
                        <th style='width: 45%' scope=\"row\">" . form_label('Disciplina : ') . "</th>
                        <th style='width: 25%' scope=\"row\">" . form_label('Dia : ') . "</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td scope=\"row\">" . form_dropdown('aula_iddisciplina', $optionsDisc, set_value('aula_iddisciplina'), array('class' => 'msg-box')) . "</td>
                        <td scope=\"row\">" . form_dropdown('aula_iddia', $optionsDia, set_value('aula_iddia'), array('class' => 'msg-box')) . "</td>
                        </tr>
                        </tbody>
                        </table>";
        echo "<table style='padding: 0px; margin: 0px' class=\"table table-borderless\">
                        <thead>
                        <tr>
                        <th scope=\"row\">" . form_label('Início : ') . "</th>
                        <th scope=\"row\">" . form_label('Fim : ') . "</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td scope=\"row\">" . form_input($dataHini) . "</td>
                        <td scope=\"row\">" . form_input($dataHfim) . "</td>
                        </tr>
                        </tbody>
                        </table>";
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Cadastrar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function cadastrarAula()
    {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        $msg = get_msg();
//        echo('Mensagem cadastrarAula: ' . $msg);

        $inserido = NULL;
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('aula_iddisciplina', 'Disciplina', 'trim|required');
        $this->form_validation->set_rules('aula_iddia', 'Dia', 'trim|required');
        $this->form_validation->set_rules('aula_inicio', 'Início', 'trim|required');
        $this->form_validation->set_rules('aula_fim', 'Fim', 'trim|required');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->aula->create_aula($dados_form['aula_inicio'], $dados_form['aula_fim'], $dados_form['aula_iddia'], $dados_form['aula_iddisciplina']);
            if ($inserido):
                set_msg('<p>Aula Cadastrada...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("aula", 'refresh');
    }

    public function updateAulaForm()
    {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        $msg = get_msg();
//        echo('Mensagem updateAulaForm: ' . $msg);

        $dados_form = $this->input->post();
        echo form_open('upAulaDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;

        $infoAllDisciplina = $this->discip->listar_todos_disciplina();
        $optionsDisc = array('' => 'Selecione...');
        foreach ($infoAllDisciplina as $tDisc) {
            $optionsDisc[$tDisc->disciplina_id] = $tDisc->disciplina_nome;
        }
        $infoAllDia = $this->aula->listar_todos_dias();
        $optionsDia = array('' => 'Selecione...');
        foreach ($infoAllDia as $tDia) {
            $optionsDia[$tDia->dia_id] = $tDia->dia_nome;
        }
//        echo form_open('upAulaDb');
//        if ($msg = get_msg()):
//            echo '<div class="msg-box">' . $msg . '</div>';
//        endif;
        $dataHini = array(
            'name' => 'aula_inicio',
            'id' => 'aula_inicio_id',
            'value' => $dados_form['aula_inicio'],
            'class' => 'form-control input_box',
            'placeholder' => 'Início'
        );
        $dataHfim = array(
            'name' => 'aula_fim',
            'id' => 'aula_fim_id',
            'value' => $dados_form['aula_fim'],
            'class' => 'form-control input_box',
            'placeholder' => 'Fim'
        );
        echo form_input('aula_id', set_value('aula_id', $dados_form['aula_id']), array('class' => 'hidden'));
        echo "<table style='padding: 0px; margin: 0px' class=\"table table-borderless\">
                        <thead>
                        <tr>
                        <th style='width: 45%' scope=\"row\">" . form_label('Disciplina : ') . "</th>
                        <th style='width: 25%' scope=\"row\">" . form_label('Dia : ') . "</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td scope=\"row\">" . form_dropdown('aula_iddisciplina', $optionsDisc, set_value('aula_iddisciplina', $dados_form['aula_iddisciplina']), array('class' => 'msg-box')) . "</td>
                        <td scope=\"row\">" . form_dropdown('aula_iddia', $optionsDia, set_value('aula_iddia', $dados_form['aula_iddia']), array('class' => 'msg-box')) . "</td>
                        </tr>
                        </tbody>
                        </table>";
        echo "<table style='padding: 0px; margin: 0px' class=\"table table-borderless\">
                        <thead>
                        <tr>
                        <th scope=\"row\">" . form_label('Início : ') . "</th>
                        <th scope=\"row\">" . form_label('Fim : ') . "</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                        <td scope=\"row\">" . form_input($dataHini) . "</td>
                        <td scope=\"row\">" . form_input($dataHfim) . "</td>
                        </tr>
                        </tbody>
                        </table>";
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Alterar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function updateAula()
    {
        $inserido = NULL;
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('aula_iddisciplina', 'Disciplina', 'trim|required');
        $this->form_validation->set_rules('aula_iddia', 'Dia', 'trim|required');
        $this->form_validation->set_rules('aula_inicio', 'Início', 'trim|required');
        $this->form_validation->set_rules('aula_fim', 'Fim', 'trim|required');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->aula->update_aula($dados_form['aula_id'], $dados_form['aula_inicio'], $dados_form['aula_fim'], $dados_form['aula_iddia'], $dados_form['aula_iddisciplina']);
            if ($inserido):
                set_msg('<p>Aula Alterada...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;

//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        $msg = get_msg();
//        echo('Mensagem updateAula: ' . $msg);
        redirect("aula", 'refresh');
    }

}

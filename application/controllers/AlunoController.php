<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AlunoController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model("AlunoModel", "aluno");
        $this->load->model("DisciplinaModel", "discip");
    }

    public function index()
    {
        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->aluno->listar_todos();
        $dados = array("titulo" => 'Aluno', "nomeLogged" => $resultadoCon1, "alunoDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_aluno', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function cadastrarAlunoForm()
    {
        echo form_open('cadAlunoDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'aluno_nome',
            'id' => 'aluno_nome_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        $dataRA = array(
            'name' => 'aluno_ra',
            'id' => 'aluno_ra_id',
            'class' => 'form-control input_box',
            'placeholder' => 'RA'
        );
        $dataTelefone = array(
            'name' => 'aluno_telefone',
            'id' => 'usuario_senha_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Telefone'
        );

        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';

        echo form_label('RA : ', 'ra');
        echo form_input($dataRA) . '<br>';

        echo form_label('Telefone : ');
        echo form_input($dataTelefone) . '<br>';

        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Cadastrar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function cadastrarAluno()
    {
        $inserido = NULL;
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('aluno_nome', 'Nome', 'trim|required|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('aluno_ra', 'RA', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('aluno_telefone', 'Telefone', 'trim|required');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->aluno->create_aluno($dados_form['aluno_ra'], $dados_form['aluno_nome'], $dados_form['aluno_telefone']);
            if ($inserido):
                set_msg('<p>Aluno Cadastrado...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("aluno", 'refresh');
    }

    public function updateAlunoForm()
    {
        $dados_form = $this->input->post();
        $disciplinas = $this->discip->listar_todos_disciplina();
        echo form_open('upAlunoDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'aluno_nome',
            'id' => 'aluno_nome_id',
            'value' => $dados_form['aluno_nome'],
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        $dataRA = array(
            'name' => 'aluno_ra',
            'id' => 'aluno_ra_id',
            'value' => $dados_form['aluno_ra'],
            'class' => 'form-control input_box',
            'placeholder' => 'RA'
        );
        $dataTelefone = array(
            'name' => 'aluno_telefone',
            'id' => 'usuario_senha_id',
            'value' => $dados_form['aluno_telefone'],
            'class' => 'form-control input_box',
            'placeholder' => 'Telefone'
        );
        echo form_input('aluno_id', set_value('aluno_id', $dados_form['aluno_id']), array('class' => 'hidden')) . "</br>" . PHP_EOL;
        echo form_input('aluno_habilitado', set_value('aluno_habilitado', $dados_form['aluno_habilitado']), array('class' => 'hidden')) . "</br>" . PHP_EOL;
        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';
        echo form_label('RA : ', 'ra');
        echo form_input($dataRA) . '<br>';
        echo form_label('Telefone : ');
        echo form_input($dataTelefone) . '<br>';
        foreach ($disciplinas as $d) {
            $checar = '';
            $verifica = $this->aluno->verifica_aulas($dados_form['aluno_id'], $d->disciplina_nome);
            if ($verifica):
                $checar = 'checked';
            endif;
            echo '<div class="checkbox">';
            echo '<label><input style="visibility: visible;margin-right:6px;" type="checkbox" name="' . $d->disciplina_nome . '" value="' . $d->disciplina_nome . '"' . $checar . '>' . $d->disciplina_nome . '</label>';
            echo '</div>';
        }
        echo "<div style = 'display:flex; justify-content:flex-end; width:100%; padding:0;' >
        <button style = 'margin-right: 10px' data-dismiss = \"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Alterar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function updateAluno()
    {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        $msg = get_msg();
//        echo('Mensagem: ' . $msg);

        $inserido = NULL;
        $aulas = '';
        set_msg(NULL);
        $dados_form = $this->input->post();
        $disciplinas = $this->discip->listar_todos_disciplina();
        foreach ($disciplinas as $d) {
            $nomeAula = str_replace(" ", "_", $d->disciplina_nome);
            if (isset($dados_form['' . $nomeAula . ''])):
                $aulas .= $dados_form['' . $nomeAula . ''] . " - \n";
//                echo ('$aulas in: ' . $aulas) . '<br>';
            endif;
        }
//        echo ('$aulas: ' . $aulas) . '<br>';
        $this->form_validation->set_rules('aluno_nome', 'Nome', 'trim|required|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('aluno_ra', 'RA', 'trim|required|min_length[5]');
        $this->form_validation->set_rules('aluno_telefone', 'Telefone', 'trim|required');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->aluno->update_aluno($dados_form['aluno_id'], $dados_form['aluno_nome'], $dados_form['aluno_ra'], $dados_form['aluno_telefone'], $aulas);
            if ($inserido):
                set_msg('<p>Aluno Alterado...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;

//        echo('Inserido: ' . $inserido);
        redirect("aluno", 'refresh');
    }

    public
    function habilitaAluno()
    {
        set_msg(NULL);
        $dados_form = $this->input->post();
        $p_habilitado = $dados_form['aluno_habilitado'];
        if ($_SESSION['usuario_perfil'] != 3):
            if ($p_habilitado == 0):
                $inserido = $this->aluno->update_habilitado($dados_form['aluno_id'], '1');
            else:
                $inserido = $this->aluno->update_habilitado($dados_form['aluno_id'], '0');
            endif;
            if ($inserido):
                set_msg('<p>Registro alterado...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("aluno", 'refresh');
    }
}

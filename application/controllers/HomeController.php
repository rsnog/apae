<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HomeController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
    }

    public function index()
    {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';
//        $msg = get_msg();
//        echo('Mensagem: ' . $msg);

        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }

    public function contactform()
    {
        $to = 'contato@nsystem.com.br';
        $subject = $_POST['subject'];
        $body = filter_var($_POST['Message'], FILTER_SANITIZE_STRING);
        $headers = 'From: contato@nsystem.com.br' . "\r\n"
            . 'Content-Type: text/plain; charset=utf-8' . "\r\n";
        if (mail($to, '=?utf-8?B?' . base64_encode($subject) . '?=', $body, $headers)) {
            echo "sent";
        } else {
            echo "fail";
        }
    }
}

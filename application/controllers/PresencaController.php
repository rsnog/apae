<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PresencaController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('Ciqrcode');
        $this->load->model("PresencaModel", "pres");
        $this->load->model("DisciplinaModel", "disc");
    }

    public function index()
    {
        $this->pres->update_presenca2('29-9-2019', 1, $this->pres->verificar_presenca('29-9-2019', 1, 'Trabalho de Conclusão de Curso II'), 'Trabalho de Conclusão de Curso II');
        $this->pres->verificar_presenca('29-9-2019', 1, 'Trabalho de Conclusão de Curso II');

        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->pres->listar_todos_alunos_presenca();
        if ($_SESSION['usuario_id'] == 1 || $_SESSION['usuario_id'] == 2) {
            $resultadoCon3 = $this->disc->listar_todos_disciplina();
        } else {
            $resultadoCon3 = $this->disc->listar_todos_disciplina_usuario($_SESSION['usuario_id']);
        }
        $dados = array("titulo" => 'Presença', "nomeLogged" => $resultadoCon1, "presencaDados" => $resultadoCon2, "disciplinaDados" => $resultadoCon3);
        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_presenca', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function loadLista()
    {
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';

        if (!isset($_POST['aluno_id'])):
            $_POST['aluno_id'] = NULL;
        endif;
        if (!isset($_POST['aluno_ra'])):
            $_POST['aluno_ra'] = NULL;
        endif;
        if (!isset($_POST['aluno_nome'])):
            $_POST['aluno_nome'] = NULL;
        endif;
        if (!isset($_POST['dia'])):
            $_POST['dia'] = date('d');
        endif;
        if (!isset($_POST['mes'])):
            $_POST['mes'] = date('m');
        endif;
        if (!isset($_POST['ano'])):
            $_POST['ano'] = date('Y');
        endif;
        if (!isset($_POST['disciplina'])):
            $_POST['disciplina'] = NULL;
        endif;
        if (!isset($_POST['dataTotal'])):
            $_POST['dataTotal'] = date('d/m/Y');
        endif;

        if ($_POST['dia'] == "") {
            $dataMySql = $_POST['dataTotal'];
        } else {
            $dataMySql = $_POST['dia'] . "/" . $_POST['mes'] . "/" . $_POST['ano'];
        }

        $dataMySqlTotal = implode("-", array_reverse(explode("/", $dataMySql)));
//        echo "Data: " . $dataMySql . '<br>';
//        echo "aluno_id: " . $_POST['aluno_id'] . '<br>';
//        echo "aluno_ra: " . $_POST['aluno_ra'] . '<br>';
//        echo "aluno_nome: " . $_POST['aluno_nome'] . '<br>';
//        echo "dataMySqlTotal: " . $dataMySqlTotal . '<br>';
//        echo "Post DataTotal: " . $_POST['dataTotal'] . '<br>';

        if (isset($_POST['presenca_check']) && $_POST['presenca_check'] == 'true') {
//            echo "presenca_check: " . $_POST['presenca_check'] . '<br>';
            $this->pres->update_presenca2($dataMySqlTotal, $_POST['aluno_id'], 1, $_POST['disciplina']);
        } else if (isset($_POST['presenca_check']) && $_POST['presenca_check'] == 'false') {
//            echo "presenca_check: " . $_POST['presenca_check'] . '<br>';
            $this->pres->update_presenca2($dataMySqlTotal, $_POST['aluno_id'], 0, $_POST['disciplina']);
        }

        if ($_POST['dia'] == "") {
            $resultado = $this->pres->listar_todos_alunos_presenca_disciplina($_POST['disciplina']);
        } else if ($_POST['dia'] != "" && $_POST['disciplina'] != "Selecione...") {
            $resultado = $this->pres->listar_todos_alunos_presenca_disciplina_data($_POST['disciplina'], $dataMySqlTotal);
        } else {
            $resultado = $this->pres->listar_todos_alunos_presenca_disciplina("Selecione...");
        }

        echo "<div class=\"table-responsive text-nowrap\">";
        echo "<table id=\"tablePresenca\" class=\"table table-striped table-advance table-hover w-auto\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th class=\"hidden\">Id</th>";
        echo "<th style='width: 10%'>RA</th>";
        echo " <th>Aluno</th>";
        echo "<th style='text-align: center' class='centered'>Presença</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";
        foreach ($resultado as $r) {
            if ($this->pres->verificar_presenca($dataMySqlTotal, $r->aluno_id, $_POST['disciplina'])):
                $inputPresenca = "<input id='" . $r->aluno_id . "' CHECKED=\"ckecked\" style=\"visibility: visible;margin-right:6px;\" type=\"checkbox\" name=\"presenteCheckNane\" onclick='post_presenca(\"tablePresenca\")'>Presente</td>";
                $verifica = 1;
            else:
                $inputPresenca = "<input id='" . $r->aluno_id . "' style=\"visibility: visible;margin-right:6px;\" type=\"checkbox\" name=\"presenteCheckNane\" onclick='post_presenca(\"tablePresenca\")'>Presente</td>";
                $verifica = 0;
            endif;

            echo "<tr>" .
                "<td class=\"hidden\">" . $r->aluno_id . "</td>" .
                "<td style='width: 10%'>" . $r->aluno_ra . "</td>" .
                "<td>" . $r->aluno_nome . "</td>" .
                "<td style='text-align: center'>" .
                $inputPresenca .
                "</tr>";
            $this->pres->update_presenca($dataMySqlTotal, $r->aluno_id, $verifica, $_POST['disciplina']);
        }
        echo "</tbody>";
        echo "</table>";
        echo " </div>";
    }

    public function updatePresenca($dt, $id)
    {
        $this->pres->update_presenca($dt, $id, TRUE, $_POST['disciplina']);
//        $this->pres->limpar_presenca();
    }


}

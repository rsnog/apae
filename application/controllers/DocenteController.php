<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DocenteController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model("DocenteModel", "docen");
    }

    public function index()
    {
        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            $this->session->set_userdata('usuario_id', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
//        $resultadoCon2 = $this->docen->listar_todos_docentes();
        $resultadoCon2 = $this->docen->listar_todos_docentes_disciplina();
//        $dados = array("titulo" => 'Docente', "nomeLogged" => $resultadoCon1, "docenteDados" => $resultadoCon2, "docenteDisciplinaDados" => $resultadoCon2);
        $dados = array("titulo" => 'Docente', "nomeLogged" => $resultadoCon1, "docenteDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_docente', $dados);
        $this->load->view('adm/adm_footer');
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DisciplinaController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model("DisciplinaModel", "discip");
        $this->load->model("DocenteModel", "docen");
        $this->load->model("UsuarioModel", "user");
    }

    public function index()
    {
        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            $this->session->set_userdata('usuario_id', '');
            redirect(base_url(), 'refresh');
        endif;
        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->discip->listar_todos_disciplina();
        $dados = array("titulo" => 'Disciplina', "nomeLogged" => $resultadoCon1, "disciplinaDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_disciplina', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function cadastrarDiscipForm()
    {
        echo form_open('cadDiscipDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'disciplina_nome',
            'id' => 'disciplina_nome_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Cadastrar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function cadastrarDiscip()
    {
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('disciplina_nome', 'Nome', 'trim|required|min_length[5]|max_length[100]');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->discip->create_disciplina($dados_form['disciplina_nome']);
            if ($inserido):
                set_msg('<p>Disciplina Cadastrada...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("disciplina", 'refresh');
    }

    public function updateDiscipForm()
    {
        $dados_form = $this->input->post();
        $infoAllDocente = $this->docen->listar_todos_docentes();
        echo form_open('upDiscipDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'disciplina_nome',
            'id' => 'disciplina_nome_id',
            'value' => $dados_form['disciplina_nome'],
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        $options = array('' => 'Selecione...');
        foreach ($infoAllDocente as $test) {
            $options[$test->usuario_id] = $test->usuario_nome;
        }
        if ($_SESSION['usuario_perfil'] == 3):
            $clUser = 'hidden';
        else:
            $clUser = 'msg-box';
        endif;
        echo form_input('disciplina_id', set_value('Disciplina_id', $dados_form['disciplina_id']), array('class' => 'hidden')) . "</br>" . PHP_EOL;
        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';
        echo form_label('Professor : ', 'professor');
        echo form_dropdown('disciplina_docente', $options, set_value('professor', $dados_form['usuario_id']), array('class' => $clUser)) . "</br>" . PHP_EOL;
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Alterar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function updateDiscip()
    {
        $inserido = NULL;
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules("disciplina_nome", "Nome", "trim|required|min_length[5]|max_length[100]");
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $inserido = $this->discip->update_disciplina($dados_form['disciplina_id'], $dados_form['disciplina_nome'], $dados_form['disciplina_docente']);
        endif;
        if ($inserido):
            set_msg('<p>Disciplina  Alterada...</p>');
        else:
            set_msg('<p>Validação falhou...</p>');
        endif;
        redirect("disciplina", 'refresh');
    }

    public function habilitaDiscip()
    {
        $dados_form = $this->input->post();
        $habilitado = $this->discip->get_habilitado($dados_form['disciplina_id']);
        if ($habilitado == 0):
            $inserido = $this->discip->update_habilitado($dados_form['disciplina_id'], '1');
        else:
            $inserido = $this->discip->update_habilitado($dados_form['disciplina_id'], '0');
        endif;
        if ($inserido):
            set_msg('<p>Disciplina  Alterada...</p>');
        else:
            set_msg('<p>Validação falhou...</p>');
        endif;
        redirect("disciplina", 'refresh');
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ServicoController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('download');
        $this->load->library('form_validation');
        $this->load->model("UsuarioModel", "user");
    }

    public function index()
    {
        $this->load->view('header');
        $this->load->view('servico');
        $this->load->view('footer');
    }

    public function logarForm()
    {
        echo form_open('logarUser');
        $dataUsuarioEmail = array(
            'name' => 'usuario_email',
            'id' => 'usuario_email_id',
            'class' => 'form-control input_box',
            'placeholder' => 'E-mail'
        );
        $dataSenha = array(
            'name' => 'usuario_senha',
            'id' => 'usuario_senha_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Senha'
        );
        echo form_label('E-mail : ');
        echo form_input($dataUsuarioEmail) . '<br>';
        echo form_label('Senha : ');
        echo form_password($dataSenha) . '<br>';
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Cancelar</button>" . form_submit('enviar', 'Logar', array('class' => 'btn btn-default', 'style' => 'width:25%; text-align: center')) . "</div>";
        echo form_close();
    }

    public function logaruser()
    {
        if (!isset($_SESSION['usuario_email'])):
            $this->session->set_userdata('usuario_email', '');
        endif;
//        if ($this->user->get_email($_SESSION['usuario_email']) != NULL):
//            redirect("administra", 'refresh');
//        endif;
        $this->form_validation->set_rules('usuario_email', 'E-mail', 'trim|required|min_length[5]|valid_email');
        $this->form_validation->set_rules('usuario_senha', 'Senha', 'trim|required|min_length[3]');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $dados_form = $this->input->post();
            if ($this->user->get_email($dados_form['usuario_email']) == $dados_form['usuario_email']):
//                if ($dados_form['senha'] == $this->user->get_senha($dados_form['email'])):
                if (password_verify($dados_form['usuario_senha'], $this->user->get_senha($dados_form['usuario_email']))):
                    if ($this->user->get_habilitado($dados_form['usuario_email']) == TRUE):

                        session_abort();
                        /* Define o limite de tempo do cache em 15 minutos */
                        session_cache_expire(15);
                        $cache_expire = session_cache_expire();
                        /* Inicia a sessão */
                        session_start();
//                        echo "As sessões em cache irão expirar em $cache_expire minutos";

                        $this->session->set_userdata('logged', TRUE);
                        $this->session->set_userdata('logoutClick', FALSE);
                        $this->session->set_userdata('usuario_email', $dados_form['usuario_email']);
                        $this->session->set_userdata('usuario_nome', $this->user->get_nome($dados_form['usuario_email']));
                        $this->session->set_userdata('usuario_perfil', $this->user->get_perfil($dados_form['usuario_email']));
                        $this->session->set_userdata('usuario_id', $this->user->get_id($_SESSION['usuario_nome'], $dados_form['usuario_email']));

                        redirect("administra", 'refresh');
                    else:
                        set_msg('<p>Usuário não habilitado...</p>');
                    endif;
                else:
                    set_msg('<p>Senha incorreta...</p>');
                endif;
            else:
                set_msg('<p>Usuário não existe...</p>');
            endif;
        endif;
        redirect("servico", 'refresh');
    }

    public function apk()
    {
        force_download("autenticação.apk", file_get_contents(base_url('assets/apk/autenticação.apk')));
    }
}

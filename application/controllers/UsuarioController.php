<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class UsuarioController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model("UsuarioModel", "user");
        $this->load->model("PermissaoModel", "perm");
    }

    public function index()
    {
//        echo '<pre>';
//        print_r($_SESSION);
//        echo '</pre>';
//        echo '<pre>';
//        print_r($_POST);
//        echo '</pre>';

        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            $this->session->set_userdata('usuario_id', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->user->listar_todos();
        $dados = array("titulo" => 'Usuários', "nomeLogged" => $resultadoCon1, "usuarioDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_usuario', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function cadastrarUserForm()
    {
        $infoAllPermissao = $this->perm->listar_todos();
        echo form_open('cadUserDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'usuario_nome',
            'id' => 'usuario_nome_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        $dataEmail = array(
            'name' => 'usuario_email',
            'id' => 'usuario_email_id',
            'class' => 'form-control input_box',
            'placeholder' => 'E-mail'
        );
        $dataSenha = array(
            'name' => 'usuario_senha',
            'id' => 'usuario_senha_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Senha'
        );
        $dataSenha2 = array(
            'name' => 'usuario_senha2',
            'id' => 'usuario_senha2_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Repita a Senha'
        );
        $options = array('' => 'Selecione...');
        foreach ($infoAllPermissao as $test) {
            if ($_SESSION['usuario_perfil'] == 1):
                $options[$test->id] = $test->permissao_nome;
            else:
                if ($test->id != 1):
                    $options[$test->id] = $test->permissao_nome;
                endif;
            endif;
        }
        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';
        echo form_label('E-mail : ');
        echo form_input($dataEmail) . '<br>';
        echo form_label('Perfil : ', 'perfil');
        echo form_dropdown('usuario_perfil', $options, set_value('perfil'), array('class' => 'msg-box')) . "</br>" . PHP_EOL;
        echo form_label('Senha : ');
        echo form_password($dataSenha) . '<br>';
        echo form_label('Repita a Senha : ');
        echo form_password($dataSenha2) . '<br>';
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Cadastrar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function cadastrarUser()
    {
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('usuario_nome', 'Nome', 'trim|required|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('usuario_email', 'E-mail', 'trim|required|min_length[5]|valid_email');
        $this->form_validation->set_rules('usuario_perfil', 'Perfil', 'trim|required');
        $this->form_validation->set_rules('usuario_senha', 'Senha', 'trim|required|min_length[3]');
        $this->form_validation->set_rules('usuario_senha2', 'Repita a senha', 'trim|required|min_length[3]|matches[usuario_senha]');
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            $senha = (password_hash($dados_form['usuario_senha'], PASSWORD_DEFAULT));
            $inserido = $this->user->create_user($dados_form['usuario_nome'], $dados_form['usuario_email'], $senha, $dados_form['usuario_perfil']);
            if ($inserido):
                set_msg('<p>Usuário Cadastrado...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("usuario", 'refresh');
    }

    public function updateUserForm()
    {
        $dados_form = $this->input->post();
        $infoAllPermissao = $this->perm->listar_todos();
        echo form_open('upUserDb');
        if ($msg = get_msg()):
            echo '<div class="msg-box">' . $msg . '</div>';
        endif;
        $dataNome = array(
            'name' => 'usuario_nome',
            'id' => 'usuario_nome_id',
            'value' => $dados_form['usuario_nome'],
            'class' => 'form-control input_box',
            'placeholder' => 'Nome'
        );
        $dataEmail = array(
            'name' => 'usuario_email',
            'id' => 'usuario_email_id',
            'value' => $dados_form['usuario_email'],
            'class' => 'form-control input_box',
            'placeholder' => 'E-mail'
        );
        $dataSenha = array(
            'name' => 'usuario_senha',
            'id' => 'usuario_senha_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Senha (Somente para alteração)'
        );
        $dataSenha2 = array(
            'name' => 'usuario_senha2',
            'id' => 'usuario_senha2_id',
            'class' => 'form-control input_box',
            'placeholder' => 'Repita a Senha (Somente para alteração)'
        );
        $options = array('' => 'Selecione...');
        foreach ($infoAllPermissao as $test) {
            if ($_SESSION['usuario_perfil'] == 1):
                $options[$test->id] = $test->permissao_nome;
            else:
                if ($test->id != 1):
                    $options[$test->id] = $test->permissao_nome;
                endif;
            endif;
        }
        if ($_SESSION['usuario_perfil'] == 3):
            $clUser = 'hidden';
        else:
            $clUser = 'msg-box';
        endif;
        echo form_input('usuario_id', set_value('perfil', $dados_form['usuario_id']), array('class' => 'hidden')) . "</br>" . PHP_EOL;
        echo form_label('Nome : ');
        echo form_input($dataNome) . '<br>';
        echo form_label('E-mail : ');
        echo form_input($dataEmail) . '<br>';
        if ($_SESSION['usuario_perfil'] != 3):
            echo form_label('Perfil : ', 'perfil');
        endif;
        echo form_dropdown('usuario_perfil', $options, set_value('perfil', $dados_form['usuario_idNivel']), array('class' => $clUser)) . "</br>" . PHP_EOL;
        echo form_label('Senha : ');
        echo form_password($dataSenha) . '<br>';
        echo form_label('Repita a Senha : ');
        echo form_password($dataSenha2) . '<br>';
        echo "<div style='display:flex; justify-content:flex-end; width:100%; padding:0;'>
        <button style='margin-right: 10px' data-dismiss=\"modal\" class=\"btn btn-danger\" type=\"button\">Fechar</button>" . form_submit('enviar', 'Alterar', array('class' => 'btn btn-success', 'style' => 'width:25%;text-align:Center;')) . "</div>";
        echo form_close();
    }

    public function updateUser()
    {
        $inserido = NULL;
        set_msg(NULL);
        $dados_form = $this->input->post();
        $this->form_validation->set_rules('usuario_nome', 'Nome', 'trim|required|min_length[5]|max_length[100]');
        $this->form_validation->set_rules('usuario_email', 'E-mail', 'trim|required|min_length[5]|valid_email');
        $this->form_validation->set_rules('usuario_perfil', 'Perfil', 'trim|required');
        if ($dados_form['usuario_senha'] != NULL):
            $this->form_validation->set_rules('usuario_senha', 'Senha', 'trim|required|min_length[3]');
            $this->form_validation->set_rules('usuario_senha2', 'Repita a senha', 'trim|required|min_length[3]|matches[usuario_senha]');
        endif;
        if ($this->form_validation->run() == FALSE):
            if (validation_errors()):
                set_msg(validation_errors());
            endif;
        else:
            if ($dados_form['usuario_senha'] != NULL):
                $senha = (password_hash($dados_form['usuario_senha'], PASSWORD_DEFAULT));
                $inserido = $this->user->update_user_senha($dados_form['usuario_id'], $dados_form['usuario_nome'], $dados_form['usuario_email'], $senha, $dados_form['usuario_perfil']);
            else:
                $inserido = $this->user->update_user($dados_form['usuario_id'], $dados_form['usuario_nome'], $dados_form['usuario_email'], $dados_form['usuario_perfil']);
            endif;
            if ($inserido):
                set_msg('<p>Usuário Alterado...</p>');
            else:
                set_msg('<p>Validação falhou...</p>');
            endif;
        endif;
        redirect("usuario", 'refresh');

    }

    public function habilitaUser()
    {
        set_msg(NULL);
        $dados_form = $this->input->post();
        $p_habilitado = $this->user->get_habilitado($dados_form['usuario_email']);
        if ($_SESSION['usuario_perfil'] != 3):
            if ($dados_form['usuario_idNivel'] != 1):
                if ($p_habilitado == 0):
                    $inserido = $this->user->update_habilitado($dados_form['usuario_email'], '1');
                else:
                    $inserido = $this->user->update_habilitado($dados_form['usuario_email'], '0');
                endif;
                if ($inserido):
                    set_msg('<p>Registro alterado...</p>');
                else:
                    set_msg('<p>Validação falhou...</p>');
                endif;
            endif;
        endif;
        redirect("usuario", 'refresh');
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class EmpresaController extends CI_Controller
{

    public function index()
    {
        $this->load->view('header');
        $this->load->view('empresa');
        $this->load->view('footer');
    }
}

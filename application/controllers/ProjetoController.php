<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ProjetoController extends CI_Controller
{

    public function index()
    {
        $this->load->view('header');
        $this->load->view('projeto');
        $this->load->view('footer');
    }
}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdmController extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->helper('form');
        $this->load->helper('url');
        $this->load->library('form_validation');
        $this->load->library('Ciqrcode');
        $this->load->model("DocenteModel", "docen");
    }

    public function index()
    {
        if (!isset($_SESSION['logged'])):
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('logoutClick', FALSE);
        endif;
        if ($_SESSION['logoutClick'] == TRUE || $_SESSION['logged'] == FALSE):
            $this->session->set_userdata('logoutClick', FALSE);
            $this->session->set_userdata('logged', FALSE);
            $this->session->set_userdata('usuario_email', '');
            $this->session->set_userdata('usuario_nome', '');
            redirect(base_url(), 'refresh');
        endif;

        $nome = explode(" ", $_SESSION['usuario_nome']);
        $primeiro_nome = $nome[0];
        $resultadoCon1 = $primeiro_nome;
        $resultadoCon2 = $this->docen->listar_todos_docentes_disciplina_aula();
        $dados = array("titulo" => 'Administração', "nomeLogged" => $resultadoCon1, "docenteDados" => $resultadoCon2);

        $this->load->view('adm/adm_header', $dados);
        $this->load->view('adm/adm_home', $dados);
        $this->load->view('adm/adm_footer');
    }

    public function logoutClick()
    {
        $this->session->set_userdata('logged', FALSE);
        $this->session->set_userdata('logoutClick', TRUE);
        $this->session->set_userdata('usuario_email', '');
        $this->session->set_userdata('usuario_nome', '');
        $this->session->set_userdata('usuario_perfil', '');
    }

    public function qrcodeGenerator()
    {
        $dados_form = $this->input->post();
        if ($dados_form['code_docente_nome'] == null || $dados_form['code_docente_nome'] == ''):
            header("Content-Type: image/png");
            $params['data'] = 'Autenticação de Presença...';
            $params['level'] = 'H';
            $params['size'] = 5;
            $params['margin'] = 4;
            $params['savename'] = FCPATH . 'tes1.png';
            $this->ciqrcode->generate($params);
            echo '<img src="' . base_url() . 'tes1.png" />';
        else:
            // get full name and user details
            //$user_details = $this->user->get_users_one($kode);
            $image_name = $dados_form['code_docente_id'] . "_" . $dados_form['code_disciplina_id'] . "_" . $dados_form['code_dia_id'] . ".png";

            // create user content
            $codeContents = "Docente:";
            $codeContents .= $dados_form['code_docente_nome'];
//        $codeContents .= $kode;
            $codeContents .= "\n";
            //$codeContents .= "$user_details->user_name";
            $codeContents .= " Aula:";
            $codeContents .= $dados_form['code_disciplina_nome'];
            //$codeContents .= "$user_details->user_address";
            $codeContents .= "\n";
            $codeContents .= " Dia:";
            $codeContents .= $dados_form['code_dia_nome'];
            //$codeContents .= $user_details->user_email;

            header("Content-Type: image/png");
            $params['data'] = $codeContents;
            $params['level'] = 'H';
            $params['size'] = 4;
            $params['margin'] = 4;
            $params['savename'] = FCPATH . $image_name;
            $this->ciqrcode->generate($params);

            echo '<img src="' . base_url() . $image_name . '" />';
//            echo base_url('/assets/qr_code/') . 'tes.png';
        endif;
//        if (unlink($image_name)):
//        endif;
    }

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ContatoController extends CI_Controller
{

    public function index()
    {
        $this->load->view('header');
        $this->load->view('contato');
        $this->load->view('footer');
    }
}
